/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.visit;

import javax.annotation.Generated;
import juast.*;

@Generated(value = { "juast.visit.GenVisitor" })
public class DefaultVisitor implements Visitor {
	private boolean defaultEnter = true, defaultLeave = true, enter = true, leave = true;
	public DefaultVisitor(boolean defaultEnter, boolean defaultLeave, boolean enter, boolean leave) {
		this.enter = enter;
		this.leave = leave;
		this.defaultEnter = defaultEnter;
		this.defaultLeave = defaultLeave;
	}
	public DefaultVisitor() {}
	public boolean enter(MethodParam node) { return enter; }
	public boolean leave(MethodParam node) { return leave; }
	public boolean enter(ReturnStmt node) { return enter; }
	public boolean leave(ReturnStmt node) { return leave; }
	public boolean enter(SwitchStmt node) { return enter; }
	public boolean leave(SwitchStmt node) { return leave; }
	public boolean enter(ParenthesizedExpr node) { return enter; }
	public boolean leave(ParenthesizedExpr node) { return leave; }
	public boolean enter(Type node) { return enter; }
	public boolean leave(Type node) { return leave; }
	public boolean enter(TypeDecl node) { return enter; }
	public boolean leave(TypeDecl node) { return leave; }
	public boolean enter(LiteralExpr node) { return enter; }
	public boolean leave(LiteralExpr node) { return leave; }
	public boolean enter(CaseNode node) { return enter; }
	public boolean leave(CaseNode node) { return leave; }
	public boolean enter(LabeledStmt node) { return enter; }
	public boolean leave(LabeledStmt node) { return leave; }
	public boolean enter(InstanceOfExpr node) { return enter; }
	public boolean leave(InstanceOfExpr node) { return leave; }
	public boolean enter(ArrayAccessExpr node) { return enter; }
	public boolean leave(ArrayAccessExpr node) { return leave; }
	public boolean enter(TryStmt node) { return enter; }
	public boolean leave(TryStmt node) { return leave; }
	public boolean enter(IdExpr node) { return enter; }
	public boolean leave(IdExpr node) { return leave; }
	public boolean enter(EmptyStmt node) { return enter; }
	public boolean leave(EmptyStmt node) { return leave; }
	public boolean enter(MethodDecl node) { return enter; }
	public boolean leave(MethodDecl node) { return leave; }
	public boolean enter(TypeParam node) { return enter; }
	public boolean leave(TypeParam node) { return leave; }
	public boolean enter(MethodInvExpr node) { return enter; }
	public boolean leave(MethodInvExpr node) { return leave; }
	public boolean enter(AssertStmt node) { return enter; }
	public boolean leave(AssertStmt node) { return leave; }
	public boolean enter(ThrowStmt node) { return enter; }
	public boolean leave(ThrowStmt node) { return leave; }
	public boolean enter(CatchNode node) { return enter; }
	public boolean leave(CatchNode node) { return leave; }
	public boolean enter(ConditionalExpr node) { return enter; }
	public boolean leave(ConditionalExpr node) { return leave; }
	public boolean enter(Import node) { return enter; }
	public boolean leave(Import node) { return leave; }
	public boolean enter(CompoundAssignExpr node) { return enter; }
	public boolean leave(CompoundAssignExpr node) { return leave; }
	public boolean enter(ForStmt node) { return enter; }
	public boolean leave(ForStmt node) { return leave; }
	public boolean enter(SynchronizedStmt node) { return enter; }
	public boolean leave(SynchronizedStmt node) { return leave; }
	public boolean enter(MemberSelectExpr node) { return enter; }
	public boolean leave(MemberSelectExpr node) { return leave; }
	public boolean enter(NewArrayExpr node) { return enter; }
	public boolean leave(NewArrayExpr node) { return leave; }
	public boolean enter(CompilationUnit node) { return enter; }
	public boolean leave(CompilationUnit node) { return leave; }
	public boolean enter(ContinueStmt node) { return enter; }
	public boolean leave(ContinueStmt node) { return leave; }
	public boolean enter(TypeCastExpr node) { return enter; }
	public boolean leave(TypeCastExpr node) { return leave; }
	public boolean enter(NewClassExpr node) { return enter; }
	public boolean leave(NewClassExpr node) { return leave; }
	public boolean enter(WhileStmt node) { return enter; }
	public boolean leave(WhileStmt node) { return leave; }
	public boolean enter(ExprStmt node) { return enter; }
	public boolean leave(ExprStmt node) { return leave; }
	public boolean enter(BinaryExpr node) { return enter; }
	public boolean leave(BinaryExpr node) { return leave; }
	public boolean enter(VarStmt node) { return enter; }
	public boolean leave(VarStmt node) { return leave; }
	public boolean enter(Block node) { return enter; }
	public boolean leave(Block node) { return leave; }
	public boolean enter(IfStmt node) { return enter; }
	public boolean leave(IfStmt node) { return leave; }
	public boolean enter(Annotation node) { return enter; }
	public boolean leave(Annotation node) { return leave; }
	public boolean enter(BreakStmt node) { return enter; }
	public boolean leave(BreakStmt node) { return leave; }
	public boolean enter(UnaryExpr node) { return enter; }
	public boolean leave(UnaryExpr node) { return leave; }
	public boolean enter(VarDecl node) { return enter; }
	public boolean leave(VarDecl node) { return leave; }
	public boolean enter(ForeachStmt node) { return enter; }
	public boolean leave(ForeachStmt node) { return leave; }
	public boolean enter(AssignExpr node) { return enter; }
	public boolean leave(AssignExpr node) { return leave; }
	public boolean defaultEnter(Node node) { return defaultEnter; }
	public boolean defaultLeave(Node node) { return defaultLeave; } 
}
