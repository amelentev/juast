/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.visit;

import javax.annotation.Generated;
import juast.*;

@Generated(value = { "juast.visit.GenVisitor" })
public interface Visitor {
	boolean enter(MethodParam node);
	boolean leave(MethodParam node);
	boolean enter(ReturnStmt node);
	boolean leave(ReturnStmt node);
	boolean enter(SwitchStmt node);
	boolean leave(SwitchStmt node);
	boolean enter(ParenthesizedExpr node);
	boolean leave(ParenthesizedExpr node);
	boolean enter(Type node);
	boolean leave(Type node);
	boolean enter(TypeDecl node);
	boolean leave(TypeDecl node);
	boolean enter(LiteralExpr node);
	boolean leave(LiteralExpr node);
	boolean enter(CaseNode node);
	boolean leave(CaseNode node);
	boolean enter(LabeledStmt node);
	boolean leave(LabeledStmt node);
	boolean enter(InstanceOfExpr node);
	boolean leave(InstanceOfExpr node);
	boolean enter(ArrayAccessExpr node);
	boolean leave(ArrayAccessExpr node);
	boolean enter(TryStmt node);
	boolean leave(TryStmt node);
	boolean enter(IdExpr node);
	boolean leave(IdExpr node);
	boolean enter(EmptyStmt node);
	boolean leave(EmptyStmt node);
	boolean enter(MethodDecl node);
	boolean leave(MethodDecl node);
	boolean enter(TypeParam node);
	boolean leave(TypeParam node);
	boolean enter(MethodInvExpr node);
	boolean leave(MethodInvExpr node);
	boolean enter(AssertStmt node);
	boolean leave(AssertStmt node);
	boolean enter(ThrowStmt node);
	boolean leave(ThrowStmt node);
	boolean enter(CatchNode node);
	boolean leave(CatchNode node);
	boolean enter(ConditionalExpr node);
	boolean leave(ConditionalExpr node);
	boolean enter(Import node);
	boolean leave(Import node);
	boolean enter(CompoundAssignExpr node);
	boolean leave(CompoundAssignExpr node);
	boolean enter(ForStmt node);
	boolean leave(ForStmt node);
	boolean enter(SynchronizedStmt node);
	boolean leave(SynchronizedStmt node);
	boolean enter(MemberSelectExpr node);
	boolean leave(MemberSelectExpr node);
	boolean enter(NewArrayExpr node);
	boolean leave(NewArrayExpr node);
	boolean enter(CompilationUnit node);
	boolean leave(CompilationUnit node);
	boolean enter(ContinueStmt node);
	boolean leave(ContinueStmt node);
	boolean enter(TypeCastExpr node);
	boolean leave(TypeCastExpr node);
	boolean enter(NewClassExpr node);
	boolean leave(NewClassExpr node);
	boolean enter(WhileStmt node);
	boolean leave(WhileStmt node);
	boolean enter(ExprStmt node);
	boolean leave(ExprStmt node);
	boolean enter(BinaryExpr node);
	boolean leave(BinaryExpr node);
	boolean enter(VarStmt node);
	boolean leave(VarStmt node);
	boolean enter(Block node);
	boolean leave(Block node);
	boolean enter(IfStmt node);
	boolean leave(IfStmt node);
	boolean enter(Annotation node);
	boolean leave(Annotation node);
	boolean enter(BreakStmt node);
	boolean leave(BreakStmt node);
	boolean enter(UnaryExpr node);
	boolean leave(UnaryExpr node);
	boolean enter(VarDecl node);
	boolean leave(VarDecl node);
	boolean enter(ForeachStmt node);
	boolean leave(ForeachStmt node);
	boolean enter(AssignExpr node);
	boolean leave(AssignExpr node);
	boolean defaultEnter(Node node);
	boolean defaultLeave(Node node);
}
