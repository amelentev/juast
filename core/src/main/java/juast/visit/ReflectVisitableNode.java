/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.visit;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashSet;

import juast.Node;
import fj.F;
import fj.data.List;
import fj.function.Booleans;

public abstract class ReflectVisitableNode implements Node {
	private List<Method> getFields() {
		Class<?> iface = this.getClass().getInterfaces()[0];
		fj.data.List<Method> res = fj.data.List.iterableList(Arrays.asList(iface.getMethods()));
		res = res.filter(new F<Method, Boolean>() {
			public Boolean f(Method m) {
				return isRightGetter(m);
			}
		});
		return res;
	}
	private F<Method, Boolean> isSimpleField = new F<Method, Boolean>() {
		public Boolean f(Method m) {
			return !Node.class.isAssignableFrom( m.getReturnType() )
				&& !isReturnNodeList(m);
		}
	};
	public List<Method> getSimpleFields() {
		return getFields().filter(isSimpleField);
	}
	public List<Method> getNodeFields() {
		return getFields().filter(Booleans.not(isSimpleField));
	}

	public boolean accept(Visitor v) {
		Class<?> iface = this.getClass().getInterfaces()[0];
		try {
			Boolean res = v.defaultEnter(this) && callVisit("enter", iface, v);
			if (res) {
				for (Method m : getNodeFields()) {
					if (isReturnNodeList(m)) {
						@SuppressWarnings("unchecked")
						Iterable<Node> r = (Iterable<Node>) m.invoke(this);
						if (r == null)
							continue;
						for (Node o : r) {
							if (o == null)
								continue;
							if (!((Node) o).accept(v))
								break;
						}
					} else {
						Node n = (Node) m.invoke(this);
						if (n == null)
							continue;
						if (!n.accept(v))
							break;
					}
				}
			}
			return v.defaultLeave(this) && callVisit("leave", iface, v);
		} catch (Exception e) {
			if (e instanceof RuntimeException)
				throw (RuntimeException)e;
			throw new RuntimeException(e);
		}
	}
	
	private static boolean isReturnNodeList(Method m) {
		if (Iterable.class.isAssignableFrom(m.getReturnType())
				&& m.getGenericReturnType() instanceof ParameterizedType) {
			ParameterizedType pt = (ParameterizedType) m.getGenericReturnType();
			Type[] tas = pt.getActualTypeArguments();
			return tas.length==1 && (tas[0] instanceof Class<?>) && Node.class.isAssignableFrom(((Class<?>)tas[0]));
		}
		return false;
	}
	
	@SuppressWarnings("serial") private static HashSet<String> deniedMethods = new HashSet<String>() {{
		add("getSimpleFields");
		add("getNodeFields");
		add("getParent");
		add("getNested");
		add("getContext");
	}};
	private static boolean isRightGetter(Method m) {
		return m.getParameterTypes().length==0
			&& !deniedMethods.contains(m.getName());
	}
	
	private boolean callVisit(String method, Class<?> node, Visitor v) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Method menter = v.getClass().getMethod(method, node);
		// for access to inline classes
		menter.setAccessible(true);
		return (Boolean) menter.invoke(v, this);
	}

	public String toString() {
		return toXML(new StringBuilder(), 0, true).toString();
	}
	// TODO: try XStream
	public StringBuilder toXML(StringBuilder sb, int ident, boolean deep) {
		StringBuilder sbChilds = new StringBuilder();
		Class<?> iface = this.getClass().getInterfaces()[0];
		ident(sb, ident);
		sb.append("<").append(iface.getSimpleName()).append(" ");
		try {
			for (Method m : getSimpleFields()) {
				Object child = m.invoke(this);
				if (child==null)
					continue;
				sb.append( getFieldName(m.getName()) ).append("=[").append(child.toString()).append("] ");
			}
			if (deep) {
				ident++;
				for (Method m : getNodeFields()) {
					if (isReturnNodeList(m)) {
						@SuppressWarnings("unchecked")
						Iterable<Node> lst = (Iterable<Node>) m.invoke(this);
						if (!lst.iterator().hasNext())
							continue;
						ident(sbChilds, ident);
						sbChilds.append("<").append( getFieldName(m.getName()) ).append(">\n");
						for (Node v : lst)
							v.toXML(sbChilds, ident+1, deep);
						ident(sbChilds, ident);
						sbChilds.append("</").append( getFieldName(m.getName()) ).append(">\n");
					} else {
						Node child = (Node) m.invoke(this);
						if (child==null)
							continue;
						ident(sbChilds, ident);
						sbChilds.append("<").append( getFieldName(m.getName()) ).append(">\n");
						child.toXML(sbChilds, ident+1, deep);
						ident(sbChilds, ident);
						sbChilds.append("</").append( getFieldName(m.getName()) ).append(">\n");
					}
				}
				ident--;
			}
			if (sbChilds.length() == 0)
				sb.append("/>\n");
			else {
				sb.append(">\n").append(sbChilds);
				ident(sb, ident);
				sb.append("</").append(iface.getSimpleName()).append(">\n");
			}
		} catch (Exception e) {
			if (e instanceof RuntimeException)
				throw (RuntimeException)e;
			throw new RuntimeException(e);
		}
		return sb;
	}
	private void ident(StringBuilder sb, int ident) {
		for (; ident>0; ident--)
			sb.append(' ');
	}

	private static String getFieldName(String method) {
		if (method.startsWith("get"))
			return method.substring(3);
		if (method.startsWith("is"))
			return method.substring(2);
		return method;
	}
}
