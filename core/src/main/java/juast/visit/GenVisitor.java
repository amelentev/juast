/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.visit;

import java.io.File;
import java.io.PrintStream;

import juast.Node;

class GenVisitor {
	public static void main(String[] args) throws Exception {
		//PrintStream out = System.out;
		try (PrintStream outi = new PrintStream(new File("src/main/java/juast/visit/Visitor.java"));
			PrintStream outd = new PrintStream(new File("src/main/java/juast/visit/DefaultVisitor.java"))) {
			outi.format(
					"package juast.visit;\n" +
					"\n" +
					"import javax.annotation.Generated;\n" +
					"import juast.*;\n" +
					"\n" +
					"@Generated(value = { \"%s\" })\n" +
					"public interface Visitor {\n",
					GenVisitor.class.getName()
			);
			outd.format(
					"package juast.visit;\n" +
					"\n" +
					"import javax.annotation.Generated;\n" +
					"import juast.*;\n" +
					"\n" +
					"@Generated(value = { \"%s\" })\n" +
					"public class DefaultVisitor implements Visitor {\n" +
					"\tprivate boolean defaultEnter = true, defaultLeave = true, enter = true, leave = true;\n" +
					"\tpublic DefaultVisitor(boolean defaultEnter, boolean defaultLeave, boolean enter, boolean leave) {\n" +
					"\t\tthis.enter = enter;\n" +
					"\t\tthis.leave = leave;\n" +
					"\t\tthis.defaultEnter = defaultEnter;\n" +
					"\t\tthis.defaultLeave = defaultLeave;\n" +
					"\t}\n" +
					"\tpublic DefaultVisitor() {}\n",
					GenVisitor.class.getName()
			);
			File f = new File("src/main/java/juast");
			for (String s :  f.list()) {
				String s1 = s.substring(0, s.length()-5);
				try {
					Class<?> cl = Class.forName("juast."+s1);
					if (cl.getAnnotation(Abstract.class) != null)
						continue;
					if (Node.class.isAssignableFrom(cl)) {
						outi.format("\tboolean enter(%s node);\n", s1);
						outi.format("\tboolean leave(%s node);\n", s1);
						outd.format("\tpublic boolean enter(%s node) { return enter; }\n", s1);
						outd.format("\tpublic boolean leave(%s node) { return leave; }\n", s1);
					}
				} catch (ClassNotFoundException e) {}
			}
			outi.println("\tboolean defaultEnter(Node node);");
			outi.println("\tboolean defaultLeave(Node node);");
			outi.println("}");
			outd.println("\tpublic boolean defaultEnter(Node node) { return defaultEnter; }");
			outd.println("\tpublic boolean defaultLeave(Node node) { return defaultLeave; } ");
			outd.println("}");
			
			outi.flush();
			outd.flush();
		}
	}
}
