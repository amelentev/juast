/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.lang.model.type.TypeKind;

public class BaseUtils {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <R, S> List<R> transform(Iterable<? extends S> list, fj.F<S, R> f) {
		if (list==null)
			return Collections.emptyList();
		return Collections.unmodifiableList(new ArrayList<R>(fj.data.List.iterableList(list).map((fj.F)f).toCollection()));
	}
	public static <R, S> List<R> transform(S[] list, fj.F<S, R> f) {
		if (list==null)
			return Collections.emptyList();
		return Collections.unmodifiableList(new ArrayList<R>(fj.data.List.list(list).map(f).toCollection()));
	}

	@SuppressWarnings("serial")
	private static final Map<String, TypeKind> types = Collections.unmodifiableMap(new HashMap<String, TypeKind>() {{
		put("boolean", TypeKind.BOOLEAN);
		put("byte", TypeKind.BYTE);
		put("char", TypeKind.CHAR);
		put("double", TypeKind.DOUBLE);
		put("float", TypeKind.FLOAT);
		put("int", TypeKind.INT);
		put("long", TypeKind.LONG);
		put("null", TypeKind.NULL);
		put("short", TypeKind.SHORT);
		put("void", TypeKind.VOID);
	}});

	public static TypeKind getTypeKind(String type) {
		TypeKind r = types.get(type);
		if (r != null)
			return r;
		if (type.endsWith("[]"))
			return TypeKind.ARRAY;
		return TypeKind.DECLARED;
	}
}
