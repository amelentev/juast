/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.common;

import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVisitor;

public class BasicTypeMirror implements TypeMirror {
	private Object t;
	public BasicTypeMirror(Object t) {
		this.t = t;
	}

	@Override
	public String toString() {
		return t.toString();
	}

	@Override
	public TypeKind getKind() {
		return BaseUtils.getTypeKind(t.toString());
	}

	@Override
	public <R, P> R accept(TypeVisitor<R, P> v, P p) {
		return v.visitUnknown(this, p);
	}

	@Override
	public boolean equals(Object obj) {
		return (obj instanceof TypeMirror) && toString().equals(obj.toString());
	}
}
