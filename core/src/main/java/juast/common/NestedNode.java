/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.common;

import juast.Node;
import juast.visit.ReflectVisitableNode;

public abstract class NestedNode<T> extends ReflectVisitableNode {
	private final T nested;
	private Node parent;
	protected Context context;

	public NestedNode(T nested, Node parent) {
		this.nested = nested;
		this.parent = parent;
		if (parent != null)
			context = parent.getContext();
	}

	@Override
	public Node getParent() {
		return parent;
	}

	@Override
	public T getNested() {
		return nested;
	}
	
	@Override
	public Context getContext() {
		return context;
	}
}
