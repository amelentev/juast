/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */


package juast.common;

import com.google.inject.ImplementedBy;
import com.google.inject.Inject;
import com.google.inject.Injector;

@ImplementedBy(Context.DefaultContext.class)
public interface Context {
	public Injector getInjector();
	
	public static class DefaultContext implements Context {
		private @Inject Injector injector;

		@Override
		public Injector getInjector() {
			return injector;
		}
		public void setInjector(Injector injector) {
			this.injector = injector;
		}
	}
}
