/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast;

import java.util.List;

import javax.lang.model.type.TypeMirror;

// TODO: complete 
public interface JuastTreeMaker {
	MethodInvExpr methodInvExpr(List<TypeMirror> typeArgs, Expr method, List<Expr> args);
	MemberSelectExpr memberSelectExpr(Expr expr, String id);
}
