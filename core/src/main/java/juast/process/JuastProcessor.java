/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.process;

import juast.CompilationUnit;

/**
 * Interface for processors.
 * To run your processor, use "-Ajuast.process=YourProcessorClass" annotation processing parameter.
 * Or add full class names to META-INF/services/juast.process.JuastProcessor file
 *  and make it available in classpath while compiling.
 */
public interface JuastProcessor {
	/**
	 * Called after syntactical analysis.
	 * AST consist only syntax info, so no type resolution.
	 * Useful for AST modification.
	 */
	void afterParse(CompilationUnit unit);
	/**
	 * Called after semantic analysis.
	 * Full type info available.
	 */
	void afterAnalyse(CompilationUnit unit);
}
