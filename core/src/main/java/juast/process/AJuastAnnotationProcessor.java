/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.process;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.Diagnostic.Kind;

import juast.CompilationUnit;
import juast.ProblemReporter;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Starting point for Pluggable Annotation Processing, JSR269
 */
public abstract class AJuastAnnotationProcessor extends AbstractProcessor {

	protected Injector injector;

	protected Iterable<JuastProcessor> getProcessors() {
		String opt = processingEnv.getOptions().get("juast.process");
		Iterable<JuastProcessor> processors;
		if (opt!=null) {
			processors = getProcessors(opt.split(","));
		} else {
			processors = ServiceLoader.load(JuastProcessor.class, this.getClass().getClassLoader());
		}
		for (JuastProcessor p : processors)
			injector.injectMembers(p);
		return processors;
	}

	private Iterable<JuastProcessor> getProcessors(String[] classes) {
		List<JuastProcessor> lst = new ArrayList<JuastProcessor>();
		for (String s : classes) {
			Class<?> cl;
			try {
				cl = Class.forName(s);
				lst.add( (JuastProcessor) cl.newInstance() );
			} catch (Exception e) {
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				processingEnv.getMessager().printMessage(Kind.ERROR, String.format("Can't load processor [%s] : %s\n%s", s, e.getMessage(), sw.toString()));
			}
		}
		return lst;
	}

	protected void afterParce(CompilationUnit unit) {
		for (JuastProcessor p : getProcessors())
			try {
				p.afterParse(unit);
			} catch (Exception e) {
				ProblemReporter pr = injector.getInstance(ProblemReporter.class);
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				pr.warning(unit, unit, String.format("Exception while afterParce unit=[%s], processor=[%s]\nException %s\n%s",
						unit.getFileName(), p.getClass().getName(), e.getMessage(), sw.toString()));
			}
	}
	protected void afterAnalyse(CompilationUnit unit) {
		for (JuastProcessor p : getProcessors())
			try {
				p.afterAnalyse(unit);
			} catch (Exception e) {
				ProblemReporter pr = injector.getInstance(ProblemReporter.class);
				StringWriter sw = new StringWriter();
				e.printStackTrace(new PrintWriter(sw));
				pr.warning(unit, unit, String.format("Exception while afterAnalyse unit=[%s], processor=[%s]\nException %s\n%s",
						unit.getFileName(), p.getClass().getName(), e.getMessage(), sw.toString()));
			}
	}

	@Override
	public synchronized void init(final ProcessingEnvironment processingEnv) {
		super.init(processingEnv);
		injector = Guice.createInjector(new AbstractModule() {
			protected void configure() {
				bind(ProcessingEnvironment.class).toInstance(processingEnv);
			}
		});
		ClassLoader cl = this.getClass().getClassLoader();
		injector = injector.createChildInjector(ServiceLoader.load(com.google.inject.Module.class, cl));
	}

	protected void log(Kind kind, String s) {
		processingEnv.getMessager().printMessage(kind, s);
	}
}
