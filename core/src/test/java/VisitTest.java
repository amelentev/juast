/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import juast.*;
import juast.common.NestedNode;
import juast.visit.DefaultVisitor;
import juast.visit.Visitor;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InOrder;

public class VisitTest {
	static class CompilationUnitImpl extends NestedNode<Void> implements CompilationUnit {
		public CompilationUnitImpl() {
			super(null, null);
		}
		@Override
		public List<Annotation> getAnnotations() {
			return null;
		}
		private Import i = new ImportImpl(this);
		@Override
		public List<Import> getImports() {
			return Arrays.asList(i);
		}
		@Override
		public String getPackageName() {
			return null;
		}
		@Override
		public List<TypeDecl> getTypes() {
			return null;
		}
		@Override
		public String getFileName() {
			return "none";
		}
	}
	static class ImportImpl extends NestedNode<Void> implements Import {
		public ImportImpl(Node p) {
			super(null, p);
		}
		@Override
		public String getName() {
			return "some.import";
		}
		@Override
		public boolean isStatic() {
			return false;
		}
	}
	
	@Test
	public void test() {
		CompilationUnit cu = new CompilationUnitImpl();
		Import imp = cu.getImports().get(0);
		DefaultVisitor dv = new DefaultVisitor();
		{
			Visitor v = spy(dv);
			cu.accept(v);
			InOrder order = inOrder(v);
			order.verify(v).defaultEnter(cu);
			order.verify(v).enter(cu);
			
			order.verify(v).defaultEnter(imp);
			order.verify(v).enter(imp);
			order.verify(v).defaultLeave(imp);
			order.verify(v).leave(imp);
			
			order.verify(v).defaultLeave(cu);
			order.verify(v).leave(cu);
			verifyNoMoreInteractions(v);
		}
		{
			Visitor v = spy(dv);
			when(v.defaultEnter(cu)).thenReturn(false);
			when(v.defaultLeave(cu)).thenReturn(false);
			cu.accept(v);
			InOrder order = inOrder(v);
			order.verify(v).defaultEnter(cu);
			order.verify(v).defaultLeave(cu);
			verifyNoMoreInteractions(v);
		}
		Assert.assertSame(cu, imp.getParent());
	}
}
