/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import java.util.List;

import juast.Block;
import juast.CatchNode;
import juast.Node;
import juast.TryStmt;
import juast.VarStmt;
import juast.common.BaseUtils;
import juast.common.NestedNode;

import com.sun.tools.javac.tree.JCTree.JCCatch;
import com.sun.tools.javac.tree.JCTree.JCTry;

import fj.F;

public class JWTryStmt extends NestedNode<JCTry> implements TryStmt {
	public JWTryStmt(JCTry nested, Node p) {
		super(nested, p);
	}

	@Override
	public Block getBody() {
		return new JWBlock(getNested().getBlock(), this);
	}

	@Override
	public List<CatchNode> getCatches() {
		return BaseUtils.transform(getNested().getCatches(), new F<JCCatch, CatchNode>() {
			public CatchNode f(JCCatch a) {
				return new JWCatchCase(a, JWTryStmt.this);
			};
		});
	}

	@Override
	public Block getFinally() {
		return new JWBlock(getNested().getFinallyBlock(), this);
	}
	
	public static class JWCatchCase extends NestedNode<JCCatch> implements CatchNode {
		public JWCatchCase(JCCatch nested, Node p) {
			super(nested, p);
		}

		@Override
		public Block getBody() {
			return new JWBlock(getNested().getBlock(), this);
		}

		@Override
		public VarStmt getCatch() {
			return new JWVarStmt(getNested().getParameter(), this);
		}
	}
}
