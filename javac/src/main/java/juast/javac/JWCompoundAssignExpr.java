/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import juast.CompoundAssignExpr;
import juast.Expr;
import juast.Node;

import com.sun.tools.javac.tree.JCTree.JCAssignOp;

public class JWCompoundAssignExpr extends AJWExpr<JCAssignOp> implements CompoundAssignExpr {

	public JWCompoundAssignExpr(JCAssignOp nested, Node p) {
		super(nested, p);
	}

	@Override
	public Expr getExpression() {
		return JCUtils.wrap(getNested().getExpression(), this);
	}

	@Override
	public String getOperator() {
		return getNested().getKind().toString();
	}

	@Override
	public Expr getVariable() {
		return JCUtils.wrap(getNested().getVariable(), this);
	}
}
