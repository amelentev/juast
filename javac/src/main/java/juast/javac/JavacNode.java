/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import juast.Node;
import juast.NodeW;
import juast.common.NestedNode;

import com.sun.source.tree.Tree;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeMaker;

public class JavacNode<T extends Tree> extends NestedNode<T> implements NodeW {
	public JavacNode(T nested, Node parent) {
		super(nested, parent);
	}

	@Override
	public void replace(Node oldChild, Node newChild) {
		JCTree nested = (JCTree) getNested(),
			oldc = (JCTree) oldChild.getNested(),
			newc = (JCTree) newChild.getNested();
		// TODO: support replace in lists
		for (Field f : nested.getClass().getFields()) {
			if (!JCTree.class.isAssignableFrom(f.getType()))
				continue;
			try {
				if (f.get(nested) == oldc) {
					f.set(nested, newc);
					break;
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	protected int getTag() {
		try {
			// javac 1.6
			Field f = JCTree.class.getField("tag");
			return f.getInt(getNested());
		} catch (NoSuchFieldException e) {
			try {
				// javac 1.7
				Method m = JCTree.class.getMethod("getTag");
				return (Integer) m.invoke(getNested());
			} catch (Exception e1) {
				throw new RuntimeException(e);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected TreeMaker getTreeMaker() {
		return getContext().getInjector().getInstance(TreeMaker.class);
	}
}
