/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import java.util.List;

import juast.Annotation;
import juast.CompilationUnit;
import juast.Import;
import juast.TypeDecl;
import juast.common.BaseUtils;
import juast.common.Context;
import juast.common.NestedNode;

import com.sun.source.tree.ImportTree;
import com.sun.source.tree.Tree;
import com.sun.tools.javac.tree.JCTree.JCClassDecl;
import com.sun.tools.javac.tree.JCTree.JCCompilationUnit;

import fj.F;

public class JWCompilationUnit extends NestedNode<JCCompilationUnit> implements CompilationUnit {
	public JWCompilationUnit(JCCompilationUnit nested, Context context) {
		super(nested, null);
		this.context = context;
	}

	@Override
	public List<Import> getImports() {
		return BaseUtils.transform(getNested().getImports(), new F<ImportTree, Import>() {
			public Import f(ImportTree a) {
				return new JWImport(a, JWCompilationUnit.this);
			}
		});
	}

	@Override
	public List<TypeDecl> getTypes() {
		return BaseUtils.transform(getNested().getTypeDecls(), new F<Tree, TypeDecl>() {
			public TypeDecl f(Tree a) {
				return new JWTypeDecl((JCClassDecl) a, JWCompilationUnit.this);
			};
		});
	}

	@Override
	public List<Annotation> getAnnotations() {
		return JCUtils.transformA(getNested().getPackageAnnotations(), this);
	}

	@Override
	public String getPackageName() {
		return getNested().getPackageName()!=null ? getNested().getPackageName().toString() : null;
	}

	@Override
	public String getFileName() {
		return getNested().getSourceFile().toUri().getPath();
	}
}
