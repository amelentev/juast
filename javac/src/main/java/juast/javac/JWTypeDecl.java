/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import java.util.List;
import java.util.Set;

import javax.lang.model.element.Modifier;
import javax.lang.model.type.TypeMirror;

import juast.Annotation;
import juast.BodyDecl;
import juast.Node;
import juast.TypeDecl;
import juast.TypeParam;
import juast.common.BaseUtils;

import com.sun.source.tree.TypeParameterTree;
import com.sun.tools.javac.code.Flags;
import com.sun.tools.javac.tree.JCTree.JCClassDecl;
import com.sun.tools.javac.tree.JCTree.JCModifiers;

import fj.F;

public class JWTypeDecl extends JavacNode<JCClassDecl> implements TypeDecl {

	public JWTypeDecl(JCClassDecl a, Node p) {
		super(a, p);
	}

	@Override
	public String getName() {
		return getNested().getSimpleName().toString();
	}

	@Override
	public List<Annotation> getAnnotations() {
		return JCUtils.transformA(getNested().getModifiers().getAnnotations(), this);
	}

	@Override
	public Set<Modifier> getModifiers() {
		return getNested().getModifiers().getFlags();
	}

	@Override
	public TypeMirror getExtends() {
		return JCUtils.transformT( getNested().getExtendsClause() );
	}

	@Override
	public List<TypeMirror> getImplements() {
		return JCUtils.transformT( getNested().getImplementsClause() );
	}

	@Override
	public List<TypeParam> getTypeParameters() {
		return BaseUtils.transform(getNested().getTypeParameters(), new F<TypeParameterTree, TypeParam>() {
			public TypeParam f(TypeParameterTree a) {
				return new JWTypeParam(a, JWTypeDecl.this);
			}
		});
	}

	@Override
	public List<BodyDecl> getBody() {
		return JCUtils.transformB(getNested().getMembers(), this);
	}

	@Override
	public TypeKind getKind() {
		long flags = ((JCModifiers) getNested().getModifiers()).flags;
		if ((flags & Flags.ANNOTATION) != 0)
			return TypeKind.AnnotationType;
		if ((flags & Flags.ENUM) != 0)
			return TypeKind.Enum;
		if ((flags & Flags.INTERFACE) != 0)
			return TypeKind.Interface;
		return TypeKind.Class;
	}
}
