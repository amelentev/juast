/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import java.util.List;

import juast.Expr;
import juast.Node;
import juast.Stmt;
import juast.CaseNode;
import juast.SwitchStmt;
import juast.common.BaseUtils;
import juast.common.NestedNode;

import com.sun.source.tree.CaseTree;
import com.sun.source.tree.SwitchTree;

import fj.F;

public class JWSwitchStmt extends NestedNode<SwitchTree> implements SwitchStmt {
	public JWSwitchStmt(SwitchTree nested, Node p) {
		super(nested, p);
	}

	@Override
	public List<CaseNode> getCases() {
		return BaseUtils.transform(getNested().getCases(), new F<CaseTree, CaseNode>() {
			public CaseNode f(CaseTree a) {
				return new JWCaseNode(a, JWSwitchStmt.this);
			}
		});
	}

	@Override
	public Expr getExpression() {
		return JCUtils.wrap(getNested().getExpression(), this);
	}
	
	public static class JWCaseNode extends NestedNode<CaseTree> implements CaseNode {
		public JWCaseNode(CaseTree nested, Node p) {
			super(nested, p);
		}

		@Override
		public Expr getCase() {
			return JCUtils.wrap(getNested().getExpression(), this);
		}

		@Override
		public List<Stmt> getStatements() {
			return JCUtils.transformS(getNested().getStatements(), this);
		}
	}
}
