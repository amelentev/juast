/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import java.io.IOException;
import java.net.URI;

import javax.tools.JavaFileObject;
import javax.tools.JavaFileObject.Kind;
import javax.tools.SimpleJavaFileObject;

import juast.CompilationUnit;
import juast.JuastParcer;
import juast.common.Context;

import com.google.inject.Inject;
import com.sun.tools.javac.main.JavaCompiler;
import com.sun.tools.javac.tree.JCTree.JCCompilationUnit;

public class JavacJuastParcer implements JuastParcer {
	@Inject Context context;
	@Inject JavaCompiler compiler;

	public CompilationUnit parce(final String text) {
		JavaFileObject jfo = new SimpleJavaFileObject(URI.create("some"), Kind.SOURCE) {
			@Override
			public CharSequence getCharContent(boolean ignoreEncodingErrors)
					throws IOException {
				return text;
			}
		};
		JCCompilationUnit cu = compiler.parse(jfo);
		return new JWCompilationUnit(cu, context);
	}
}
