/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import juast.common.NestedNode;

import java.util.HashMap;
import java.util.Map;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.ExecutableElement;

import juast.Annotation;
import juast.Expr;
import juast.Node;

import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.tools.javac.tree.JCTree.JCAnnotation;

public class JWAnnotation extends NestedNode<JCAnnotation> implements Annotation {
	public JWAnnotation(JCAnnotation nested, Node p) {
		super(nested, p);
	}

	@Override
	public String getTypeName() {
		if (getNested().type != null)
			return getNested().type.toString();
		return getNested().getAnnotationType().toString();
	}

	@Override
	public Map<String, Expr> getValues() {
		Map<String, Expr> res = new HashMap<String, Expr>();
		for (ExpressionTree a : getNested().getArguments()) {
			if (a instanceof AssignmentTree) {
				AssignmentTree as = (AssignmentTree) a;
				res.put(as.getVariable().toString(), JCUtils.wrap(as.getExpression(), this));
			} else
				res.put(defaultValue, JCUtils.wrap(a, this));
		}
		return res;
	}
	
	public static class JWAnnotationMirror extends NestedNode<AnnotationMirror> implements Annotation {
		public JWAnnotationMirror(AnnotationMirror nested, Node parent) {
			super(nested, parent);
		}

		@Override
		public String getTypeName() {
			return getNested().getAnnotationType().toString();
		}

		@Override
		public Map<String, Expr> getValues() {
			Map<String, Expr> res = new HashMap<String, Expr>();
			for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> e : getNested().getElementValues().entrySet()) {
				res.put(e.getKey().toString(), null);
				// TODO: e.getValue()
			}
			return res;
		}
	}
}
