/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import java.util.List;

import javax.lang.model.type.TypeMirror;

import juast.Expr;
import juast.NewClassExpr;
import juast.Node;
import juast.TypeDecl;

import com.sun.tools.javac.tree.JCTree.JCNewClass;

public class JWNewClassExpr extends AJWExpr<JCNewClass> implements NewClassExpr {

	public JWNewClassExpr(JCNewClass nested, Node p) {
		super(nested, p);
	}

	@Override
	public List<Expr> getArguments() {
		return JCUtils.transformE(getNested().getArguments(), this);
	}

	@Override
	public TypeDecl getClassBody() {
		return getNested().getClassBody()!=null ? new JWTypeDecl(getNested().getClassBody(), this) : null;
	}
	
	@Override
	public Expr getEnclosingExpression() {
		return JCUtils.wrap(getNested().getEnclosingExpression(), this);
	}

	@Override
	public String getClassName() {
		return getNested().getIdentifier().toString();
	}

	@Override
	public List<TypeMirror> getTypeArguments() {
		return JCUtils.transformT(getNested().getTypeArguments());
	}
}
