/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.tools.FileObject;
import javax.tools.JavaFileManager;
import javax.tools.JavaFileManager.Location;
import javax.tools.JavaFileObject;

import juast.JuastParcer;
import juast.JuastTreeMaker;
import juast.ProblemReporter;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.sun.tools.javac.main.JavaCompiler;
import com.sun.tools.javac.processing.JavacFiler;
import com.sun.tools.javac.tree.TreeInfo;
import com.sun.tools.javac.tree.TreeMaker;
import com.sun.tools.javac.util.Context;
import com.sun.tools.javac.util.Name.Table;

public class JavacModule extends AbstractModule {
	@Provides TreeInfo getTreeInfo(Context context) {
		return TreeInfo.instance(context);
	}
	@Provides JavaCompiler getCompiler(Context context) {
		return JavaCompiler.instance(context);
	}
	@Provides TreeMaker getTreeMaker(Context context) {
		return TreeMaker.instance(context);
	}
	@Provides JavaFileManager getFileManager(Context context) {
		return context.get(JavaFileManager.class);
	}
	@Provides Table getNames(Context context) {
		try {
			// 1.6
			Method m = Table.class.getMethod("instance", Context.class);
			return (Table) m.invoke(null, context);
		} catch (NoSuchMethodException e) {
			// not 1.6
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		try {
			// 1.7
			Method m = Class.forName("com.sun.tools.javac.util.Names").getMethod("instance", Context.class);
			Object o = m.invoke(null, context);
			return (Table) o.getClass().getField("table").get(o);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	@Provides Filer getFiler(ProcessingEnvironment pe, Context context) {
		return new JavacCorrectedFiler((JavacFiler) pe.getFiler(), context.get(JavaFileManager.class));
	}
	/**
	 * {@link JavacFiler} replacement that throw right exception in {@link #getResource(Location, CharSequence, CharSequence)} instead of NPE.
	 */
	private static class JavacCorrectedFiler implements Filer {
		private JavacFiler filer;
		private JavaFileManager fileManager;
		private JavacCorrectedFiler(JavacFiler filer, JavaFileManager fileManager) {
			this.filer = filer;
			this.fileManager = fileManager;
		}
		@Override public JavaFileObject createSourceFile(CharSequence name, Element... originatingElements) throws IOException {
			return filer.createSourceFile(name, originatingElements);
		}
		@Override public JavaFileObject createClassFile(CharSequence name, Element... originatingElements) throws IOException {
			return filer.createClassFile(name, originatingElements);
		}
		@Override public FileObject createResource(Location location, CharSequence pkg, CharSequence relativeName, Element... originatingElements) throws IOException {
			return filer.createResource(location, pkg, relativeName, originatingElements);
		}
		@Override public FileObject getResource(Location location, CharSequence pkg, CharSequence relativeName) throws IOException {
			if (fileManager.hasLocation(location))
				return filer.getResource(location, pkg, relativeName);
			throw new IllegalArgumentException("Unsupported location: "+location);
		}
	}

	protected void configure() {
		bind(ProblemReporter.class).to(JavacProblemReporter.class);
		bind(JuastTreeMaker.class).to(JavacJuastTreeMaker.class);
		bind(JuastParcer.class).to(JavacJuastParcer.class);
	}
}
