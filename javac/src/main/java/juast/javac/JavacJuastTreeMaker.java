/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import java.util.List;

import javax.lang.model.type.TypeMirror;

import juast.Expr;
import juast.JuastTreeMaker;
import juast.MemberSelectExpr;
import juast.MethodInvExpr;

import com.google.inject.Inject;
import com.sun.tools.javac.tree.JCTree.JCExpression;
import com.sun.tools.javac.tree.JCTree.JCFieldAccess;
import com.sun.tools.javac.tree.JCTree.JCMethodInvocation;
import com.sun.tools.javac.util.Name.Table;

public class JavacJuastTreeMaker implements JuastTreeMaker {
	@Inject com.sun.tools.javac.tree.TreeMaker tm;
	@Inject Table table;
	
	JCExpression from(Expr e) {
		return (JCExpression) e.getNested();
	}
	com.sun.tools.javac.util.List<JCExpression> from(List<Expr> le) {
		com.sun.tools.javac.util.List<JCExpression> res = com.sun.tools.javac.util.List.nil();
		for (Expr e : le) {
			res = res.append(from(e));
		}
		return res;
	}
	
	@Override
	public MethodInvExpr methodInvExpr(List<TypeMirror> typeArgs, Expr method, List<Expr> args) {
		// TODO: typeArgs
		JCMethodInvocation nested = tm.Apply(null, from(method), from(args));
		return new JWMethodInvExpr(nested, null);
	}

	@Override
	public MemberSelectExpr memberSelectExpr(Expr expr, String id) {
		JCFieldAccess nested = tm.Select(from(expr), table.fromChars(id.toCharArray(), 0, id.length()));
		return new JWMemberSelectExpr(nested, null);
	}
}
