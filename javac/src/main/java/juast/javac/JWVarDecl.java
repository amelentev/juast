/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import java.util.List;
import java.util.Set;

import javax.lang.model.element.Modifier;
import javax.lang.model.type.TypeMirror;

import juast.Annotation;
import juast.Expr;
import juast.Node;
import juast.VarDecl;

import com.sun.tools.javac.tree.JCTree.JCVariableDecl;

public class JWVarDecl extends JavacNode<JCVariableDecl> implements VarDecl {
	public JWVarDecl(JCVariableDecl nested, Node p) {
		super(nested, p);
	}

	@Override
	public List<Annotation> getAnnotations() {
		return JCUtils.transformA(getNested().getModifiers().getAnnotations(), this);
	}

	@Override
	public Set<Modifier> getModifiers() {
		return getNested().getModifiers().getFlags();
	}

	@Override
	public TypeMirror getType() {
		return JCUtils.transformT( getNested().vartype );
	}

	@Override
	public String getVar() {
		return getNested().getName().toString();
	}

	@Override
	public Expr getInit() {
		return JCUtils.wrap(getNested().getInitializer(), this);
	}
}
