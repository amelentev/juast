/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import juast.BinaryExpr;
import juast.Expr;
import juast.Node;

import com.sun.tools.javac.tree.JCTree.JCBinary;
import com.sun.tools.javac.tree.TreeInfo;

public class JWBinaryExpr extends AJWExpr<JCBinary> implements
		BinaryExpr {
	public JWBinaryExpr(JCBinary nested, Node p) {
		super(nested, p);
	}

	@Override
	public Expr getLeftOperand() {
		return JCUtils.wrap(getNested().getLeftOperand(), this);
	}

	@Override
	public String getOperator() {
		TreeInfo treeInfo = getContext().getInjector().getInstance(TreeInfo.class);
		return treeInfo.operatorName( getTag() ).toString();
	}

	@Override
	public Expr getRightOperand() {
		return JCUtils.wrap(getNested().getRightOperand(), this);
	}
}
