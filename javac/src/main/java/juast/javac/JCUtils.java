/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import java.util.Collections;
import java.util.List;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.type.TypeMirror;

import juast.Annotation;
import juast.BodyDecl;
import juast.EmptyStmt;
import juast.Expr;
import juast.Node;
import juast.Stmt;
import juast.common.BaseUtils;
import juast.common.BasicTypeMirror;
import juast.common.NotImplementedException;

import com.sun.source.tree.ArrayAccessTree;
import com.sun.source.tree.AssertTree;
import com.sun.source.tree.AssignmentTree;
import com.sun.source.tree.BinaryTree;
import com.sun.source.tree.BlockTree;
import com.sun.source.tree.BreakTree;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.CompoundAssignmentTree;
import com.sun.source.tree.ConditionalExpressionTree;
import com.sun.source.tree.ContinueTree;
import com.sun.source.tree.DoWhileLoopTree;
import com.sun.source.tree.EmptyStatementTree;
import com.sun.source.tree.EnhancedForLoopTree;
import com.sun.source.tree.ExpressionStatementTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.ForLoopTree;
import com.sun.source.tree.IdentifierTree;
import com.sun.source.tree.IfTree;
import com.sun.source.tree.InstanceOfTree;
import com.sun.source.tree.LabeledStatementTree;
import com.sun.source.tree.LiteralTree;
import com.sun.source.tree.MemberSelectTree;
import com.sun.source.tree.MethodInvocationTree;
import com.sun.source.tree.MethodTree;
import com.sun.source.tree.NewArrayTree;
import com.sun.source.tree.NewClassTree;
import com.sun.source.tree.ParenthesizedTree;
import com.sun.source.tree.ReturnTree;
import com.sun.source.tree.StatementTree;
import com.sun.source.tree.SwitchTree;
import com.sun.source.tree.SynchronizedTree;
import com.sun.source.tree.ThrowTree;
import com.sun.source.tree.Tree;
import com.sun.source.tree.TryTree;
import com.sun.source.tree.TypeCastTree;
import com.sun.source.tree.UnaryTree;
import com.sun.source.tree.VariableTree;
import com.sun.source.tree.WhileLoopTree;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.JCTree.JCAnnotation;
import com.sun.tools.javac.tree.JCTree.JCArrayAccess;
import com.sun.tools.javac.tree.JCTree.JCAssign;
import com.sun.tools.javac.tree.JCTree.JCAssignOp;
import com.sun.tools.javac.tree.JCTree.JCBinary;
import com.sun.tools.javac.tree.JCTree.JCClassDecl;
import com.sun.tools.javac.tree.JCTree.JCConditional;
import com.sun.tools.javac.tree.JCTree.JCEnhancedForLoop;
import com.sun.tools.javac.tree.JCTree.JCFieldAccess;
import com.sun.tools.javac.tree.JCTree.JCIdent;
import com.sun.tools.javac.tree.JCTree.JCInstanceOf;
import com.sun.tools.javac.tree.JCTree.JCLiteral;
import com.sun.tools.javac.tree.JCTree.JCMethodDecl;
import com.sun.tools.javac.tree.JCTree.JCMethodInvocation;
import com.sun.tools.javac.tree.JCTree.JCNewArray;
import com.sun.tools.javac.tree.JCTree.JCNewClass;
import com.sun.tools.javac.tree.JCTree.JCParens;
import com.sun.tools.javac.tree.JCTree.JCTry;
import com.sun.tools.javac.tree.JCTree.JCTypeCast;
import com.sun.tools.javac.tree.JCTree.JCUnary;
import com.sun.tools.javac.tree.JCTree.JCVariableDecl;

import fj.F;

public class JCUtils {
	private JCUtils() {}

	public static List<Annotation> transformA(Iterable<? extends JCAnnotation> list, final Node p) {
		return BaseUtils.transform(list, new F<JCAnnotation, Annotation>() {
			public Annotation f(JCAnnotation a) {
				return new JWAnnotation(a, p);
			}
		});
	}
	public static List<Annotation> getAnnotationsFromSymbol(Symbol sym, final Node p) {
		if (sym == null) return Collections.emptyList();
		return BaseUtils.transform(sym.attributes_field, new fj.F<AnnotationMirror, Annotation>() {
			public Annotation f(AnnotationMirror a) {
				return new JWAnnotation.JWAnnotationMirror(a, p);
			}
		});
		// return transformA(tm.Annotations(sym.attributes_field), p); // TODO: treemaker bug: "@some.annotation" transforms "@.some.annotation"
	}

	public static Expr wrap(ExpressionTree node, Node p) {
		if (node == null)
			return null;
		if (node instanceof ArrayAccessTree)
			return new JWArrayAccessExpr((JCArrayAccess) node, p);
		if (node instanceof AssignmentTree)
			return new JWAssignExpr((JCAssign) node, p);
		if (node instanceof BinaryTree)
			return new JWBinaryExpr((JCBinary) node, p);
		if (node instanceof CompoundAssignmentTree)
			return new JWCompoundAssignExpr((JCAssignOp) node, p);
		if (node instanceof ConditionalExpressionTree)
			return new JWConditionalExpr((JCConditional) node, p);
		if (node instanceof IdentifierTree)
			return new JWIdExpr((JCIdent) node, p);
		if (node instanceof InstanceOfTree)
			return new JWInstanceOfExpr((JCInstanceOf) node, p);
		if (node instanceof LiteralTree)
			return new JWLiteralExpr((JCLiteral) node, p);
		if (node instanceof MemberSelectTree)
			return new JWMemberSelectExpr((JCFieldAccess) node, p);
		if (node instanceof MethodInvocationTree)
			return new JWMethodInvExpr((JCMethodInvocation) node, p);
		if (node instanceof NewArrayTree)
			return new JWNewArrayExpr((JCNewArray) node, p);
		if (node instanceof NewClassTree)
			return new JWNewClassExpr((JCNewClass) node, p);
		if (node instanceof ParenthesizedTree)
			return new JWParenthesizedExpr((JCParens) node, p);
		if (node instanceof TypeCastTree)
			return new JWTypeCastExpr((JCTypeCast) node, p);
		if (node instanceof UnaryTree)
			return new JWUnaryExpr((JCUnary) node, p);
		throw new NotImplementedException(); 
	}

	public static List<Expr> transformE(List<? extends ExpressionTree> list, final Node p) {
		return BaseUtils.transform(list, new F<ExpressionTree, Expr>() {
			public Expr f(ExpressionTree a) {
				return wrap(a, p);
			}
		});
	}

	public static List<BodyDecl> transformB(List<? extends Tree> body, final Node p) {
		return BaseUtils.transform(body, new F<Tree, BodyDecl>() {
			public BodyDecl f(Tree a) {
				if (a instanceof MethodTree) {
					return new JWMethodDecl((JCMethodDecl) a, p);
				} else if (a instanceof VariableTree) {
					return new JWVarDecl((JCVariableDecl) a, p);
				} else if (a instanceof ClassTree) {
					return new JWTypeDecl((JCClassDecl) a, p);
				} else if (a instanceof BlockTree) {
					return new JWMethodDecl.JWInitializer((BlockTree) a, p);
				} else
					throw new NotImplementedException();
			}
		});
	}

	public static Stmt wrap(StatementTree node, Node p) {
		if (node == null)
			return null;
		if (node instanceof AssertTree)
			return new JWAssertStmt((AssertTree) node, p);
		if (node instanceof BlockTree)
			return new JWBlock((BlockTree) node, p);
		if (node instanceof BreakTree)
			return new JWBreakStmt((BreakTree) node, p);
		if (node instanceof ClassTree)
			return new JWTypeDecl((JCClassDecl) node, p);
		if (node instanceof ContinueTree)
			return new JWContinueStmt((ContinueTree) node, p);
		if (node instanceof DoWhileLoopTree)
			return new JWDoWhileStmt((DoWhileLoopTree) node, p);
		if (node instanceof EmptyStmt)
			return new JWEmptyStmt((EmptyStatementTree) node, p);
		if (node instanceof EnhancedForLoopTree)
			return new JWForeachStmt((JCEnhancedForLoop) node, p);
		if (node instanceof ExpressionStatementTree)
			return new JWExprStmt((ExpressionStatementTree) node, p);
		if (node instanceof ForLoopTree)
			return new JWForStmt((ForLoopTree) node, p);
		if (node instanceof IfTree)
			return new JWIfStmt((IfTree) node, p);
		if (node instanceof LabeledStatementTree)
			return new JWLabeledStmt((LabeledStatementTree) node, p);
		if (node instanceof ReturnTree)
			return new JWReturnStmt((ReturnTree) node, p);
		if (node instanceof SwitchTree)
			return new JWSwitchStmt((SwitchTree) node, p);
		if (node instanceof SynchronizedTree)
			return new JWSynchronizedStmt((SynchronizedTree) node, p);
		if (node instanceof ThrowTree)
			return new JWThrowStmt((ThrowTree) node, p);
		if (node instanceof TryTree)
			return new JWTryStmt((JCTry) node, p);
		if (node instanceof VariableTree)
			return new JWVarStmt((JCVariableDecl) node, p);
		if (node instanceof WhileLoopTree)
			return new JWWhileStmt((WhileLoopTree) node, p);
		throw new NotImplementedException();
	}

	public static List<Stmt> transformS(List<? extends StatementTree> list, final Node p) {
		return BaseUtils.transform(list, new F<StatementTree, Stmt>() {
			public Stmt f(StatementTree a) {
				return wrap(a, p);
			}
		});
	}

	static protected List<TypeMirror> transformT(Iterable<? extends JCTree> types) {
		return BaseUtils.transform(types, new fj.F<JCTree, TypeMirror>() {
			public TypeMirror f(JCTree a) {
				return transformT(a);
			}
		});
	}

	static protected TypeMirror transformT(JCTree tree) {
		return tree==null ? null : tree.type!=null ? tree.type : new BasicTypeMirror(tree);
	}
}
