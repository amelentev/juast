/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import java.lang.reflect.Field;

import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.JavaFileObject;

import juast.CompilationUnit;
import juast.Node;
import juast.ProblemReporter;

import com.google.inject.Inject;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.JCTree.JCCompilationUnit;
import com.sun.tools.javac.util.Log;

public class JavacProblemReporter implements ProblemReporter {
	@Inject ProcessingEnvironment pe;

	@Override
	public void warning(CompilationUnit cu, Node node, String msg) {
		JavacProcessingEnvironment pe = (JavacProcessingEnvironment) this.pe;
		Log log;
		try {
			Field f = pe.getMessager().getClass().getDeclaredField("log");
			f.setAccessible(true);
			log = (Log) f.get(pe.getMessager());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		JavaFileObject oldsource = null,
			newsource = ((JCCompilationUnit) cu.getNested()).getSourceFile();
		if (newsource != null)
			oldsource = log.useSource(newsource);
		try {
			log.warning( ((JCTree) node.getNested()).pos(), "proc.messager", msg);
		} finally {
			if (oldsource != null)
				log.useSource(oldsource);
		}
	}
}
