/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import javax.lang.model.type.TypeMirror;

import juast.Expr;
import juast.InstanceOfExpr;
import juast.Node;

import com.sun.tools.javac.tree.JCTree.JCInstanceOf;

public class JWInstanceOfExpr extends AJWExpr<JCInstanceOf> implements InstanceOfExpr {
	public JWInstanceOfExpr(JCInstanceOf nested, Node p) {
		super(nested, p);
	}

	@Override
	public Expr getExpression() {
		return JCUtils.wrap(getNested().getExpression(), this);
	}

	@Override
	public TypeMirror getType() {
		return JCUtils.transformT(getNested().clazz);
	}
}
