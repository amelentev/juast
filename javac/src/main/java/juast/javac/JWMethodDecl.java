/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.lang.model.element.Modifier;
import javax.lang.model.type.TypeMirror;

import juast.Annotation;
import juast.Block;
import juast.Expr;
import juast.MethodDecl;
import juast.MethodParam;
import juast.Node;
import juast.TypeParam;
import juast.common.BaseUtils;
import juast.common.NestedNode;

import com.sun.source.tree.BlockTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.TypeParameterTree;
import com.sun.source.tree.VariableTree;
import com.sun.tools.javac.tree.JCTree.JCMethodDecl;
import com.sun.tools.javac.tree.JCTree.JCVariableDecl;

import fj.F;

public class JWMethodDecl extends JavacNode<JCMethodDecl> implements MethodDecl {
	public JWMethodDecl(JCMethodDecl nested, Node p) {
		super(nested, p);
	}

	@Override
	public Set<Modifier> getModifiers() {
		return getNested().getModifiers().getFlags();
	}

	@Override
	public List<Annotation> getAnnotations() {
		return JCUtils.transformA(getNested().getModifiers().getAnnotations(), this);
	}

	@Override
	public List<TypeParam> getTypeParameters() {
		return BaseUtils.transform(getNested().getTypeParameters(), new F<TypeParameterTree, TypeParam>() {
			public TypeParam f(TypeParameterTree a) {
				return new JWTypeParam(a, JWMethodDecl.this);
			}
		});
	}

	@Override
	public TypeMirror getReturnType() {
		return JCUtils.transformT(getNested().getReturnType());
	}

	@Override
	public String getName() {
		return getNested().getName().toString();
	}

	@Override
	public List<MethodParam> getParameters() {
		return BaseUtils.transform(getNested().getParameters(), new F<VariableTree, MethodParam>() {
			public MethodParam f(VariableTree a) {
				return new JWMethodParam((JCVariableDecl) a, JWMethodDecl.this);
			}
		});
	}

	@Override
	public List<TypeMirror> getThrows() {
		return JCUtils.transformT(getNested().getThrows());
	}

	@Override
	public Block getBody() {
		return getNested().getBody() != null ? new JWBlock(getNested().getBody(), this) : null;
	}

	@Override
	public Expr getDefaultValue() {
		return JCUtils.wrap( (ExpressionTree) getNested().getDefaultValue(), this );
	}
	
	@Override
	public MethodKind getKind() {
		if (getNested().getName().contentEquals("<init>"))
			return MethodKind.Constructor;
		// TODO: AnnotationMethod detection
		return MethodKind.Method;
	}
	
	public static class JWInitializer extends NestedNode<BlockTree> implements MethodDecl {
		public JWInitializer(BlockTree nested, Node p) {
			super(nested, p);
		}

		@Override
		public Block getBody() {
			return new JWBlock(getNested(), this);
		}

		@Override
		public Expr getDefaultValue() {
			return null;
		}

		@Override
		public String getName() {
			return null;
		}

		@Override
		public List<MethodParam> getParameters() {
			return null;
		}

		@Override
		public TypeMirror getReturnType() {
			return null;
		}

		@Override
		public List<TypeMirror> getThrows() {
			return Collections.emptyList();
		}

		@Override
		public List<TypeParam> getTypeParameters() {
			return Collections.emptyList();
		}

		@Override
		public List<Annotation> getAnnotations() {
			return Collections.emptyList();
		}

		@Override
		public Set<Modifier> getModifiers() {
			if (getNested().isStatic())
				return new HashSet<Modifier>(Arrays.asList(Modifier.STATIC));
			return Collections.emptySet();
		}

		@Override
		public MethodKind getKind() {
			return MethodKind.Initializer;
		}
	}
}
