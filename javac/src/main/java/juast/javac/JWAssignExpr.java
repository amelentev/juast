/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import juast.AssignExpr;
import juast.Expr;
import juast.Node;

import com.sun.tools.javac.tree.JCTree.JCAssign;

public class JWAssignExpr extends AJWExpr<JCAssign> implements AssignExpr {
	public JWAssignExpr(JCAssign nested, Node p) {
		super(nested, p);
	}

	@Override
	public Expr getExpr() {
		return JCUtils.wrap(getNested().getExpression(), this);
	}

	@Override
	public Expr getVar() {
		return JCUtils.wrap(getNested().getVariable(), this);
	}
}
