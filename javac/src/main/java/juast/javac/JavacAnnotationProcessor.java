/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import java.lang.reflect.Field;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;

import juast.CompilationUnit;
import juast.common.BaseUtils;
import juast.common.Context;
import juast.common.Context.DefaultContext;
import juast.process.AJuastAnnotationProcessor;

import com.google.inject.AbstractModule;
import com.sun.source.util.TaskEvent;
import com.sun.source.util.TaskEvent.Kind;
import com.sun.source.util.TaskListener;
import com.sun.source.util.TreePath;
import com.sun.source.util.Trees;
import com.sun.tools.javac.main.JavaCompiler;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.tree.JCTree.JCCompilationUnit;

public class JavacAnnotationProcessor extends AJuastAnnotationProcessor {

	private JavacProcessingEnvironment getProcessingEnv() {
		return (JavacProcessingEnvironment) processingEnv;
	}
	private JavaCompiler compiler;

	@Override
	public synchronized void init(final ProcessingEnvironment processingEnv) {
		super.init(processingEnv);
		log(javax.tools.Diagnostic.Kind.NOTE, "juast: found Sun Java Compiler");
		injector = injector.createChildInjector(new JavacContextModule(getProcessingEnv().getContext()), new JavacModule());
	}

	Map<JCCompilationUnit, CompilationUnit> map = new IdentityHashMap<JCCompilationUnit, CompilationUnit>();

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		if (roundEnv.getRootElements().size()==0)
			return false;
		final Trees trees = Trees.instance(getProcessingEnv());
		final DefaultContext context = (DefaultContext) injector.getInstance(Context.class);
		context.setInjector(injector);
		final List<CompilationUnit> units = BaseUtils.transform(roundEnv.getRootElements(), new fj.F<Element, CompilationUnit>() {
			public CompilationUnit f(Element a) {
				TreePath tp = trees.getPath(a);
				JCCompilationUnit cu = (JCCompilationUnit) tp.getCompilationUnit();
				JWCompilationUnit junit = new JWCompilationUnit(cu, context);
				map.put(cu, junit);
				return junit;
			}
		});
		for (CompilationUnit unit : units)
			afterParce(unit);
		
		compiler = injector.getInstance(JavaCompiler.class);
		TaskListener oldTaskListener; 
		Field ftaskListener = null;
		try {
			ftaskListener = compiler.getClass().getDeclaredField("taskListener");
			ftaskListener.setAccessible(true);
			oldTaskListener = (TaskListener) ftaskListener.get(compiler);
		} catch (NoSuchFieldException e) {
			// 1.7
			oldTaskListener = getProcessingEnv().getContext().get(TaskListener.class);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		TaskListener newTaskListener = new NestedTaskListener(oldTaskListener);
		if (ftaskListener != null)
		try {
			ftaskListener.set(compiler, newTaskListener);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		getProcessingEnv().getContext().put(TaskListener.class, (TaskListener)null);
		getProcessingEnv().getContext().put(TaskListener.class, newTaskListener);
		
		try { // protect classloader from closing
			Field f = getProcessingEnv().getClass().getDeclaredField("processorClassLoader");
			f.setAccessible(true);
			ClassLoader cl = (ClassLoader) f.get(getProcessingEnv());
			cl = new ClassLoader(cl) {};
			f.set(getProcessingEnv(), cl);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return false;
	}

	@Override
	protected void afterAnalyse(CompilationUnit unit) {
		// update javac Context, because it was cleared after annotation processing.
		final com.sun.tools.javac.util.Context javacContext;
		try {
			Field f = compiler.getClass().getDeclaredField("delegateCompiler");
			f.setAccessible(true);
			JavaCompiler delegateCompiler = (JavaCompiler) f.get(compiler);
			if (delegateCompiler != null)
				compiler = delegateCompiler;
			f = compiler.getClass().getDeclaredField("context");
			f.setAccessible(true);
			javacContext = (com.sun.tools.javac.util.Context) f.get(compiler);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		injector = injector.getParent().createChildInjector(new JavacContextModule(javacContext), new JavacModule());
		// update juast context & units to use new javac context
		final DefaultContext context = (DefaultContext) injector.getInstance(Context.class);
		context.setInjector(injector);
		unit = new JWCompilationUnit((JCCompilationUnit) unit.getNested(), context);
		super.afterAnalyse(unit);
	}

	private class JavacContextModule extends AbstractModule {
		private com.sun.tools.javac.util.Context context;
		public JavacContextModule(com.sun.tools.javac.util.Context context) {
			this.context = context;
		}
		@Override
		protected void configure() {
			bind(com.sun.tools.javac.util.Context.class).toInstance(context);
		}
	}
	private class NestedTaskListener implements TaskListener {
		private TaskListener next;
		private NestedTaskListener(TaskListener next) {
			this.next = next;
		}
		@Override
		public void started(TaskEvent e) {
			if (next != null)
				next.started(e);
		}
		@Override
		public void finished(TaskEvent e) {
			if (next != null)
				next.finished(e);
			if (e.getKind() == Kind.ANALYZE) {
				final DefaultContext context = (DefaultContext) injector.getInstance(Context.class);
				context.setInjector(injector);
				afterAnalyse( new JWCompilationUnit((JCCompilationUnit) e.getCompilationUnit(), context ));
			}
		}
	}
}
