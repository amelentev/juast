/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.javac;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.lang.model.type.TypeMirror;

import juast.Annotation;
import juast.Expr;
import juast.MethodInvExpr;
import juast.Node;
import juast.common.NotImplementedException;

import com.sun.tools.javac.code.Symbol.MethodSymbol;
import com.sun.tools.javac.code.Symbol.VarSymbol;
import com.sun.tools.javac.tree.JCTree.JCExpression;
import com.sun.tools.javac.tree.JCTree.JCFieldAccess;
import com.sun.tools.javac.tree.JCTree.JCIdent;
import com.sun.tools.javac.tree.JCTree.JCMethodInvocation;
import com.sun.tools.javac.tree.TreeInfo;

public class JWMethodInvExpr extends AJWExpr<JCMethodInvocation> implements MethodInvExpr {
	public JWMethodInvExpr(JCMethodInvocation nested, Node p) {
		super(nested, p);
	}

	public List<Expr> getArgs() {
		return JCUtils.transformE(getNested().getArguments(), this);
	}

	public String getMethod() {
		JCExpression e = getNested().getMethodSelect();
		if (e instanceof JCIdent)
			return ((JCIdent) e).getName().toString();
		else if (e instanceof JCFieldAccess)
			return ((JCFieldAccess) e).getIdentifier().toString();
		else
			throw new NotImplementedException();
	}

	@Override
	public Expr getReceiver() {
		JCExpression e = getNested().getMethodSelect();
		if (e instanceof JCIdent)
			return null;
		else if (e instanceof JCFieldAccess)
			return JCUtils.wrap(((JCFieldAccess) e).getExpression(), this);
		else
			throw new NotImplementedException();
	}

	public List<TypeMirror> getTypeArgs() {
		return JCUtils.transformT(getNested().getTypeArguments());
	}

	private MethodSymbol getSymbol() {
		return (MethodSymbol) TreeInfo.symbol(getNested().meth);
	}

	public List<Annotation> getAnnotations() {
		return JCUtils.getAnnotationsFromSymbol(getSymbol(), JWMethodInvExpr.this);
	}

	@Override
	public List<List<Annotation>> getArgsAnnotations() {
		MethodSymbol sym = getSymbol();
		if (sym == null)
			return Collections.emptyList();
		List<List<Annotation>> res = new ArrayList<List<Annotation>>();
		for (VarSymbol p : sym.params())
			res.add( JCUtils.getAnnotationsFromSymbol(p, JWMethodInvExpr.this) );
		int argsSize = getArgs().size();
		while (res.size() < argsSize) // duplicate annotation for varArgs params
			res.add(res.get(res.size()-1));
		return res;
	}
}
