/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package test;

import java.io.File;
import java.io.FileReader;
import java.lang.annotation.*;
import java.math.BigInteger;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;

import juast.CompilationUnit;
import juast.JuastParcer;
import juast.MethodInvExpr;
import juast.TypeDecl;
import juast.javac.JavacModule;
import juast.visit.DefaultVisitor;

import org.apache.commons.io.IOUtils;

import com.google.inject.*;
import com.sun.tools.javac.processing.JavacProcessingEnvironment;
import com.sun.tools.javac.util.Context;

public class JavacParserProg extends Object {
	
	public static void main(String[] args) throws Exception {
		// some program
		String text = IOUtils.toString(new FileReader(new File("src/test/java/" 
				+JavacParserProg.class.getName().replace('.', '/')+".java")));
		
		Injector injector = Guice.createInjector(new AbstractModule() {
			Context context = new Context();
			@Provides Context getContext() {
				return context;
			}
			public @Provides ProcessingEnvironment getProcessingEnvironment() {
				return new JavacProcessingEnvironment(getContext(), null);
			}
			@Override
			protected void configure() {
			}
		}, new JavacModule());
		CompilationUnit cu = injector.getInstance(JuastParcer.class).parce(text);

		System.out.println(cu.toString());
		
		cu.accept(new MyVisitor());
		
		System.out.println(JavacParserProg.class);
		System.out.println(-JavacParserProg.a++);
	}
	
	static int a = 1 + 3;
	
	public JavacParserProg() {
		a++;
	}
	

	public static class MyVisitor extends DefaultVisitor {
		@Override
		public boolean enter(TypeDecl node) {
			return super.enter(node);
		}
		
		@Override
		public boolean enter(MethodInvExpr node) {
			return super.enter(node);
		}
	}
	
	BigInteger testVarArg(int ...args) {
		BigInteger a = BigInteger.valueOf(args[0]), b = BigInteger.valueOf(args[1]);
		return a.add(b);
	}
	
	@TestAnnotation(value="qwe")
	public interface ITest {
		
	}
	
	enum ETest {
		
	}
	
	@Documented
	@Target({ElementType.TYPE})
	@Retention(RetentionPolicy.RUNTIME)
	public @interface TestAnnotation {
		String value();
		int value1() default 2;
	}
	
	public void draw(List<? super Integer> lst) {
		JavacParserProg test = this;
		test.testVarArg();
		new JavacParserProg().testVarArg();
	}
	
	enum TestEnum {
		test1("1"),
		test2("2");
		
		String s;
		private TestEnum(String s) {
			this.s = s;
		}
	}
}
