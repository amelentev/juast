/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ServiceLoader;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import juast.CompilationUnit;
import juast.Expr;
import juast.Node;
import juast.ProblemReporter;
import juast.javac.JavacAnnotationProcessorProxy;
import juast.process.JuastProcessor;
import juast.visit.DefaultVisitor;

import org.junit.Test;

import com.google.inject.Inject;

/**
 * @see src/test/java/META-INF/juast.process.JuastProcessor
 */
public class JavacProcessorTest implements JuastProcessor {
	@Test
	public void testFindProcessor() {
		int count = 0;
		for(Processor p : ServiceLoader.load(Processor.class)) {
			assertEquals(JavacAnnotationProcessorProxy.class, p.getClass());
			count++;
		}
		assertEquals(count, 1);
	}

	String opts = "-source 1.6 src/test/java/"+JavacProcessorTest.class.getName().replace('.', '/')+".java";

	@Test
	public void testViaServiceProvider() {
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		wasParse = wasAnalyse = false;
		compiler.run(System.in, System.out, System.err, 
				opts.split(" "));
		assertTrue(wasParse);
		assertTrue(wasAnalyse);
	}

	@Test
	public void testViaParam() {
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		wasParse = wasAnalyse = false;
		compiler.run(System.in, System.out, System.err, 
				(opts+" -Ajuast.process="+JavacProcessorTest.class.getName()).split(" "));
		assertTrue(wasParse);
		assertTrue(wasAnalyse);
	}
	// static because juast creates new instance of processor
	static boolean wasParse = false;
	static boolean wasAnalyse = false;

	@Inject ProcessingEnvironment pe;
	@Inject ProblemReporter pr;

	@Override
	public void afterParse(CompilationUnit unit) {
		assertNotNull(unit);
		assertNotNull(pe);
		assertNotNull(pr);
		unit.accept(new DefaultVisitor() {
			@Override
			public boolean defaultEnter(Node node) {
				assertNotNull(node);
				if (node instanceof Expr)
					assertNull( ((Expr) node).getResolvedType() );
				return true;
			}
		});
		wasParse = true;
	}

	@Override
	public void afterAnalyse(CompilationUnit unit) {
		assertNotNull(unit);
		assertNotNull(pe);
		assertNotNull(pr);
		unit.accept(new DefaultVisitor() {
			@Override
			public boolean defaultEnter(Node node) {
				assertNotNull(node);
				if (node instanceof Expr)
					assertNotNull( ((Expr) node).getResolvedType() );
				return true;
			}
		});
		wasAnalyse = true;
	}
}
