/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package test;

import juast.CompilationUnit;
import juast.JuastParcer;
import juast.TypeDecl;
import juast.visit.DefaultVisitor;
import juast.visit.Visitor;

import java.io.File;
import java.io.FileReader;
import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.net.URI;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.ServiceLoader;

import javax.annotation.processing.ProcessingEnvironment;

import juast.common.Context;
import juast.common.Context.DefaultContext;
import juast.ecj.EcjJuastParcer;
import juast.ecj.EcjModule;

import org.apache.commons.io.IOUtils;
import org.eclipse.jdt.internal.compiler.apt.dispatch.BaseProcessingEnvImpl;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import static juast.ecj.EcjUtils.*;

/**
 * Used for print AST of some code to stdout.
 */
@SuppressWarnings("unused")
public class EcjParcerProg extends Object implements Runnable {
	static int a = 1 + 3, b;
	
	public EcjParcerProg() {}
	
	EcjParcerProg(CompilationUnit cu) {
		System.out.println("hi");
		System.out.println(Integer.class instanceof Class<?>);
		System.out.println(-EcjParcerProg.a++);
		cu.accept((Visitor) new MyVisitor() {
			@Override
			protected Object clone() throws CloneNotSupportedException {
				return super.clone();
			}
		});
		assert a==4 : b==3;
	}
	
	static {
		//
	}
	
	public static void main(String[] args) throws Exception {
		{
			Enumeration<URL> en = EcjParcerProg.class.getClassLoader().getResources("META-INF/services/javax.annotation.processing.Processor");
			while (en.hasMoreElements())
				System.out.println(en.nextElement());
		}
		// some program
		String text = IOUtils.toString(new FileReader(new File("src/test/java/" + EcjParcerProg.class.getName().replace('.', '/')+".java")));

		Injector injector = Guice.createInjector(new AbstractModule() {
			protected void configure() {
				bind(ProcessingEnvironment.class).toInstance(new BaseProcessingEnvImpl() {
					public Locale getLocale() {
						return Locale.getDefault();
					}
				});
			}
		}, new EcjModule());
		CompilationUnit cu = injector.getInstance(EcjJuastParcer.class).parce(text);
		System.out.println(cu.toString());
	}
	
	public static class MyVisitor extends DefaultVisitor {
		MyVisitor I = this;
		@Override
		public boolean enter(TypeDecl node) {
			this.new InnerClass(1, "2") {};
			I.new InnerClass(2, "1") {};
			return super.enter(node);
		}

		private class InnerClass {
			public InnerClass(int a, String b) {
			}
		}
	}
	
	

	<T extends Number> void testVarArg(List<? extends T> lst, @SuppressWarnings("unchecked") T ...args) {
		
	}

	@TestAnnotation(value="qwe")
	public interface ITest {
		
	}
	
	@Documented
	@Target({ElementType.TYPE})
	@Retention(RetentionPolicy.RUNTIME)
	public @interface TestAnnotation {
		String value();
		int value1() default 2;
	}
	
	public void draw(List<? super Integer> lst) {
	}

	@Override
	public void run() {
	}
	
	enum TestEnum {
		test1("1"),
		test2("2");
		
		String s;
		private TestEnum(String s) {
			this.s = s;
		}
	}
}
