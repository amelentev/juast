/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic.Kind;

import juast.CompilationUnit;
import juast.MethodParam;
import juast.common.BaseUtils;
import juast.common.Context;
import juast.common.Context.DefaultContext;
import juast.process.AJuastAnnotationProcessor;
import juast.visit.DefaultVisitor;

import org.eclipse.jdt.internal.compiler.Compiler;
import org.eclipse.jdt.internal.compiler.apt.dispatch.BaseProcessingEnvImpl;
import org.eclipse.jdt.internal.compiler.ast.Argument;
import org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration;
import org.eclipse.jdt.internal.compiler.lookup.TagBits;

import fj.Effect;

public class EcjAnnotationProcessor extends AJuastAnnotationProcessor {

	private List<CompilationUnit> units;
	private Map<CompilationUnitDeclaration, CompilationUnit> map = new IdentityHashMap<CompilationUnitDeclaration, CompilationUnit>();

	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		super.init(processingEnv);
		log(Kind.NOTE, "juast: found Eclipse Compiller");
		injector = injector.createChildInjector(new EcjModule());
		final Compiler compiler = ((BaseProcessingEnvImpl) processingEnv).getCompiler();
		final DefaultContext context = (DefaultContext) injector.getInstance(Context.class);
		context.setInjector(injector);
		
		for (int i=0; i<compiler.unitsToProcess.length; i++) {
			CompilationUnitDeclarationClone cl = new CompilationUnitDeclarationClone(compiler.unitsToProcess[i], 
				new Effect<CompilationUnitDeclaration>() {
					@Override
					public void e(CompilationUnitDeclaration a) {
						afterAnalyse(map.get(a));
					}
				});
			compiler.unitsToProcess[i] = cl;
		}
		this.units = BaseUtils.transform(compiler.unitsToProcess, new fj.F<CompilationUnitDeclaration, CompilationUnit>(){
        	public CompilationUnit f(CompilationUnitDeclaration a) {
        		compiler.parser.getMethodBodies(a);
        		EWCompilationUnit eunit = new EWCompilationUnit(a, context);
        		map.put(a, eunit);
        		return eunit;
        	}
        });
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		if (roundEnv.getRootElements().size() == 0)
			return false;
		for (CompilationUnit unit : units) {
			unit.accept(new DefaultVisitor() {
				@Override
				public boolean enter(MethodParam node) {
					if (node.getAnnotations().size()>0)
						// HACK: Support of annotations on method parameters
						((Argument)node.getNested()).binding.tagBits &= ~TagBits.AnnotationResolved;
					return true;
				}
			});
			afterParce(unit);
		}
		return false;
	}
}
