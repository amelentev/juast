/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.io.IOException;

import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Element;
import javax.tools.FileObject;
import javax.tools.JavaFileManager.Location;
import javax.tools.JavaFileObject;
import javax.tools.StandardLocation;

import juast.JuastParcer;
import juast.JuastTreeMaker;
import juast.ProblemReporter;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

public class EcjModule extends AbstractModule {
	/**
	 * {@link IdeFilerImpl} replacement that support {@link Filer#getResource(Location, CharSequence, CharSequence)} on {@link StandardLocation#SOURCE_PATH}
	 */
	public static class IdeFiler implements Filer {
		private ProcessingEnvironment pe;

		public IdeFiler(ProcessingEnvironment pe) {
			this.pe = pe;
		}
		@Override public JavaFileObject createSourceFile(CharSequence name, Element... originatingElements) throws IOException {
			return pe.getFiler().createSourceFile(name, originatingElements);
		}
		@Override public JavaFileObject createClassFile(CharSequence name, Element... originatingElements) throws IOException {
			return pe.getFiler().createClassFile(name, originatingElements);
		}
		@Override public FileObject createResource(Location location, CharSequence pkg, CharSequence relativeName, Element... originatingElements) throws IOException {
			return pe.getFiler().createResource(location, pkg, relativeName, originatingElements);
		}
		@Override public FileObject getResource(Location location, CharSequence pkg, CharSequence relativeName) throws IOException {
			if (location == StandardLocation.SOURCE_PATH) {
				String path = (pkg==null || "".equals(pkg)) ? relativeName.toString() : pkg+"/"+relativeName;
				try { // return new IdeInputFileObject(pe.getProject().getFile(path));
					// we haven't access to Eclipse IDE classes, so use reflection
					Object project = pe.getClass().getMethod("getProject").invoke(pe);
					Object file = project.getClass().getMethod("getFile", String.class).invoke(project, path);
					return (FileObject) pe.getClass().getClassLoader().loadClass("org.eclipse.jdt.internal.apt.pluggable.core.filer.IdeInputFileObject")
						.getConstructors()[0].newInstance(file);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			} else
				return pe.getFiler().getResource(location, pkg, relativeName);
		}
	}
	boolean isIde(ProcessingEnvironment pe) {
		if ("org.eclipse.jdt.internal.compiler.apt.dispatch.BatchProcessingEnvImpl" // we can't use Batch* classes directly because Eclipse IDE doesn't know it
				.equals(pe.getClass().getName()))
			return false;
		return true;
	}
	@Provides Filer getFiler(ProcessingEnvironment pe) throws Exception {
		if (isIde(pe)) {
			return new IdeFiler(pe);
		} else
			return pe.getFiler();
	}

	protected void configure() {
		bind(ProblemReporter.class).to(EcjProblemReporter.class);
		bind(JuastTreeMaker.class).to(EcjJuastTreeMaker.class);
		bind(JuastParcer.class).to(EcjJuastParcer.class);
	}
}
