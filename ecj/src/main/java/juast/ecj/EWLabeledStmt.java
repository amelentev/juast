/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import juast.Node;
import juast.Stmt;
import juast.common.NestedNode;

import org.eclipse.jdt.internal.compiler.ast.LabeledStatement;

public class EWLabeledStmt extends NestedNode<LabeledStatement> implements
		juast.LabeledStmt {

	public EWLabeledStmt(LabeledStatement nested, Node p) {
		super(nested, p);
	}

	@Override
	public String getLabel() {
		return new String(getNested().label);
	}

	@Override
	public Stmt getStatement() {
		return EcjUtils.wrap(getNested().statement, this);
	}
}
