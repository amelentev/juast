/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.lang.model.element.Modifier;

import juast.Annotation;
import juast.Node;
import juast.common.BaseUtils;
import juast.common.NotImplementedException;

import org.eclipse.jdt.internal.compiler.ast.AbstractVariableDeclaration;
import org.eclipse.jdt.internal.compiler.ast.AllocationExpression;
import org.eclipse.jdt.internal.compiler.ast.ArrayAllocationExpression;
import org.eclipse.jdt.internal.compiler.ast.ArrayInitializer;
import org.eclipse.jdt.internal.compiler.ast.ArrayReference;
import org.eclipse.jdt.internal.compiler.ast.AssertStatement;
import org.eclipse.jdt.internal.compiler.ast.Assignment;
import org.eclipse.jdt.internal.compiler.ast.BinaryExpression;
import org.eclipse.jdt.internal.compiler.ast.Block;
import org.eclipse.jdt.internal.compiler.ast.BreakStatement;
import org.eclipse.jdt.internal.compiler.ast.CastExpression;
import org.eclipse.jdt.internal.compiler.ast.ClassLiteralAccess;
import org.eclipse.jdt.internal.compiler.ast.CompoundAssignment;
import org.eclipse.jdt.internal.compiler.ast.ConditionalExpression;
import org.eclipse.jdt.internal.compiler.ast.ContinueStatement;
import org.eclipse.jdt.internal.compiler.ast.DoStatement;
import org.eclipse.jdt.internal.compiler.ast.EmptyStatement;
import org.eclipse.jdt.internal.compiler.ast.ExplicitConstructorCall;
import org.eclipse.jdt.internal.compiler.ast.Expression;
import org.eclipse.jdt.internal.compiler.ast.FieldReference;
import org.eclipse.jdt.internal.compiler.ast.ForStatement;
import org.eclipse.jdt.internal.compiler.ast.ForeachStatement;
import org.eclipse.jdt.internal.compiler.ast.IfStatement;
import org.eclipse.jdt.internal.compiler.ast.InstanceOfExpression;
import org.eclipse.jdt.internal.compiler.ast.LabeledStatement;
import org.eclipse.jdt.internal.compiler.ast.Literal;
import org.eclipse.jdt.internal.compiler.ast.MessageSend;
import org.eclipse.jdt.internal.compiler.ast.PostfixExpression;
import org.eclipse.jdt.internal.compiler.ast.PrefixExpression;
import org.eclipse.jdt.internal.compiler.ast.QualifiedNameReference;
import org.eclipse.jdt.internal.compiler.ast.ReturnStatement;
import org.eclipse.jdt.internal.compiler.ast.SingleNameReference;
import org.eclipse.jdt.internal.compiler.ast.Statement;
import org.eclipse.jdt.internal.compiler.ast.SwitchStatement;
import org.eclipse.jdt.internal.compiler.ast.SynchronizedStatement;
import org.eclipse.jdt.internal.compiler.ast.ThisReference;
import org.eclipse.jdt.internal.compiler.ast.ThrowStatement;
import org.eclipse.jdt.internal.compiler.ast.TryStatement;
import org.eclipse.jdt.internal.compiler.ast.TypeDeclaration;
import org.eclipse.jdt.internal.compiler.ast.TypeParameter;
import org.eclipse.jdt.internal.compiler.ast.UnaryExpression;
import org.eclipse.jdt.internal.compiler.ast.WhileStatement;
import org.eclipse.jdt.internal.compiler.classfmt.ClassFileConstants;

import fj.F;

public class EcjUtils {
	private EcjUtils() {}

	public static String getPkg(char[][] pkg) {
		StringBuilder sb = new StringBuilder();
		for (char[] c : pkg) {
			if (sb.length() > 0)
				sb.append('.');
			sb.append(c);
		}
		return sb.toString();
	}

	public static Set<Modifier> getModifiers(int mods) {
		Set<Modifier> res = new HashSet<Modifier>();
		if ((mods & ClassFileConstants.AccPublic) > 0)
			res.add(Modifier.PUBLIC);
		if ((mods & ClassFileConstants.AccProtected) > 0)
			res.add(Modifier.PROTECTED);
		if ((mods & ClassFileConstants.AccPrivate) > 0)
			res.add(Modifier.PRIVATE);
		if ((mods & ClassFileConstants.AccAbstract) > 0)
			res.add(Modifier.ABSTRACT);
		if ((mods & ClassFileConstants.AccStatic) > 0)
			res.add(Modifier.STATIC);
		if ((mods & ClassFileConstants.AccFinal) > 0)
			res.add(Modifier.FINAL);
		if ((mods & ClassFileConstants.AccTransient) > 0)
			res.add(Modifier.TRANSIENT);
		if ((mods & ClassFileConstants.AccVolatile) > 0)
			res.add(Modifier.VOLATILE);
		if ((mods & ClassFileConstants.AccSynchronized) > 0)
			res.add(Modifier.SYNCHRONIZED);
		if ((mods & ClassFileConstants.AccNative) > 0)
			res.add(Modifier.NATIVE);
		if ((mods & ClassFileConstants.AccStrictfp) > 0)
			res.add(Modifier.STRICTFP);
		return res;
	}

	public static List<Annotation> transformA(org.eclipse.jdt.internal.compiler.ast.Annotation[] list, final Node p) {
		return BaseUtils.transform(list, new F<org.eclipse.jdt.internal.compiler.ast.Annotation, Annotation>() {
			public Annotation f(org.eclipse.jdt.internal.compiler.ast.Annotation a) {
				return new EWAnnotation(a, p);
			}
		});
	}

	public static List<juast.TypeParam> transformTP(TypeParameter[] tp, final Node p) {
		return BaseUtils.transform(tp, new F<TypeParameter, juast.TypeParam>() {
			public juast.TypeParam f(TypeParameter a) {
				return new EWTypeParam(a, p);
			}
		});
	}

	public static List<juast.Stmt> transformS(Statement[] list, final Node p) {
		return BaseUtils.transform(list, new fj.F<Statement, juast.Stmt>() {
			public juast.Stmt f(Statement s) {
				return wrap(s, p);
			}
		});
	}

	public static List<juast.Expr> transformE(Expression[] list, final Node p) {
		return BaseUtils.transform(list, new fj.F<Expression, juast.Expr>() {
			public juast.Expr f(Expression e) {
				return wrap(e, p);
			}
		});
	}

	public static juast.Expr wrap(Expression e, Node p) {
		if (e==null)
			return null;
		if (e instanceof AllocationExpression)
			return new EWNewClassExpr((AllocationExpression) e, p);
		if (e instanceof ArrayAllocationExpression)
			return new EWNewArrayExpr((ArrayAllocationExpression) e, p);
		if (e instanceof ArrayInitializer)
			return new EWNewArrayExpr((ArrayInitializer) e, p);
		if (e instanceof PostfixExpression)
			return new EWUnaryExpr.Increment((CompoundAssignment) e, p);
		if (e instanceof PrefixExpression)
			return new EWUnaryExpr.Increment((CompoundAssignment) e, p);
		if (e instanceof CompoundAssignment)
			return new EWCompoundAssignExpr((CompoundAssignment) e, p);
		if (e instanceof Assignment)
			return new EWAssignExpr((Assignment) e, p);
		if (e instanceof CastExpression)
			return new EWTypeCastExpr((CastExpression) e, p);
		if (e instanceof ClassLiteralAccess)
			return new EWMemberSelectExpr.ClassLiteral((ClassLiteralAccess) e, p);
		if (e instanceof Literal)
			return new EWLiteralExpr((Literal) e, p);
		if (e instanceof MessageSend)
			return new EWMethodInvExpr((MessageSend) e, p);
		if (e instanceof BinaryExpression)
			return new EWBinaryExpr((BinaryExpression) e, p);
		if (e instanceof ConditionalExpression)
			return new EWConditionalExpr((ConditionalExpression) e, p);
		if (e instanceof InstanceOfExpression)
			return new EWInstanceOfExpr((InstanceOfExpression) e, p);
		if (e instanceof UnaryExpression)
			return new EWUnaryExpr((UnaryExpression) e, p);
		if (e instanceof ArrayReference)
			return new EWArrayAccessExpr((ArrayReference) e, p);
		if (e instanceof SingleNameReference)
			return new EWIdExpr((SingleNameReference) e, p);
		if (e instanceof ThisReference)
			return new EWIdExpr.This((ThisReference) e, p);
		if (e instanceof QualifiedNameReference)
			return new EWMemberSelectExpr.QNE((QualifiedNameReference) e, p);
		if (e instanceof FieldReference)
			return new EWMemberSelectExpr.Field((FieldReference) e, p);
		throw new NotImplementedException();
	}

	public static juast.Stmt wrap(Statement s, Node p) {
		if (s==null)
			return null;
		if (s instanceof AbstractVariableDeclaration)
			return new EWVarStmt((AbstractVariableDeclaration) s, p);
		if (s instanceof AssertStatement)
			return new EWAssertStmt((AssertStatement) s, p);
		if (s instanceof Block)
			return new EWBlock((Block) s, p);
		if (s instanceof BreakStatement)
			return new EWBreakStmt((BreakStatement) s, p);
		if (s instanceof ContinueStatement)
			return new EWContinueStmt((ContinueStatement) s, p);
		if (s instanceof DoStatement)
			return new EWWhileStmt.DoWhile((DoStatement) s, p);
		if (s instanceof EmptyStatement)
			return new EWEmptyStmt((EmptyStatement) s, p);
		if (s instanceof ExplicitConstructorCall)
			throw new NotImplementedException();  
		if (s instanceof Expression)
			return new EWExprStmt((Expression) s, p);
		if (s instanceof ForeachStatement)
			return new EWForeachStmt((ForeachStatement) s, p);
		if (s instanceof ForStatement)
			return new EWForStmt((ForStatement) s, p);
		if (s instanceof IfStatement)
			return new EWIfStmt((IfStatement) s, p);
		if (s instanceof LabeledStatement)
			return new EWLabeledStmt((LabeledStatement) s, p);
		if (s instanceof ReturnStatement)
			return new EWReturnStmt((ReturnStatement) s, p);
		if (s instanceof SynchronizedStatement)
			return new EWSynchronizedStmt((SynchronizedStatement) s, p);
		if (s instanceof TryStatement)
			return new EWTryStmt((TryStatement) s, p);
		if (s instanceof SwitchStatement)
			return new EWSwitchStmt((SwitchStatement) s, p);
		if (s instanceof ThrowStatement)
			return new EWThrowStmt((ThrowStatement) s, p);
		if (s instanceof TypeDeclaration)
			return new EWTypeDecl((TypeDeclaration) s, p);
		if (s instanceof WhileStatement)
			return new EWWhileStmt((WhileStatement) s, p);
		throw new NotImplementedException();
	}
}
