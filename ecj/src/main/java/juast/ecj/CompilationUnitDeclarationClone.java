/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration;

import fj.Effect;

public class CompilationUnitDeclarationClone extends CompilationUnitDeclaration {
	private Effect<CompilationUnitDeclaration> callback;
	public CompilationUnitDeclarationClone(CompilationUnitDeclaration unit, Effect<CompilationUnitDeclaration> callback) {
		super(unit.problemReporter, unit.compilationResult, unit.sourceEnd+1);
		try {
			for (Field f : unit.getClass().getDeclaredFields()) {
				if (Modifier.isFinal( f.getModifiers() ))
					continue;
				f.setAccessible(true);
				f.set(this, f.get(unit));
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		this.callback = callback;
	}

	@Override
	public void analyseCode() {
		super.analyseCode();
		callback.e(this);
	}
}
