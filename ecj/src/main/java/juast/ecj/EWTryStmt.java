/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import juast.Block;
import juast.CatchNode;
import juast.Node;
import juast.VarStmt;
import juast.common.NestedNode;
import org.eclipse.jdt.internal.compiler.ast.TryStatement;

public class EWTryStmt extends NestedNode<TryStatement> implements
		juast.TryStmt {

	public EWTryStmt(TryStatement nested, Node p) {
		super(nested, p);
	}

	@Override
	public Block getBody() {
		return new EWBlock( getNested().tryBlock, this );
	}

	@Override
	public List<CatchNode> getCatches() {
		if (getNested().catchBlocks == null)
			return Collections.emptyList();
		List<CatchNode> res = new ArrayList<CatchNode>();
		for (int i=0; i<getNested().catchBlocks.length; i++)
			res.add(new EWCatchCase(i, this));
		return Collections.unmodifiableList(res);
	}

	@Override
	public Block getFinally() {
		return new EWBlock( getNested().finallyBlock, this );
	}
	
	public class EWCatchCase extends NestedNode<Integer> implements CatchNode {
		public EWCatchCase(Integer ind, Node parent) {
			super(ind, parent);
		}

		@Override
		public Block getBody() {
			return new EWBlock(EWTryStmt.this.getNested().catchBlocks[getNested()], this);
		}

		@Override
		public VarStmt getCatch() {
			return new EWVarStmt(EWTryStmt.this.getNested().catchArguments[getNested()], this);
		}
	}
}
