/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import javax.annotation.processing.ProcessingEnvironment;

import juast.Node;

import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jdt.internal.compiler.apt.dispatch.BaseProcessingEnvImpl;
import org.eclipse.jdt.internal.compiler.ast.ASTNode;
import org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration;
import org.eclipse.jdt.internal.compiler.problem.ProblemReporter;
import org.eclipse.jdt.internal.compiler.problem.ProblemSeverities;

import com.google.inject.Inject;

public class EcjProblemReporter implements juast.ProblemReporter {
	@Inject ProcessingEnvironment pe;

	@Override
	public void warning(juast.CompilationUnit cu, Node node, String msg) {
		ProblemReporter epr = ((BaseProcessingEnvImpl) pe).getCompiler().problemReporter;
		CompilationUnitDeclaration ecu = (CompilationUnitDeclaration) cu.getNested();
		ASTNode enode = (ASTNode) node.getNested();
		epr.handle(IProblem.Unclassified, new String[]{msg}, 0, new String[]{msg}, ProblemSeverities.Warning, 
				enode.sourceStart(), enode.sourceEnd(), ecu, ecu.compilationResult());
	}
}
