/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import juast.Expr;
import juast.Node;
import juast.Stmt;
import juast.WhileStmt;
import juast.common.NestedNode;

import org.eclipse.jdt.internal.compiler.ast.DoStatement;
import org.eclipse.jdt.internal.compiler.ast.WhileStatement;

public class EWWhileStmt extends NestedNode<WhileStatement> implements
		WhileStmt {

	public EWWhileStmt(WhileStatement nested, Node p) {
		super(nested, p);
	}

	@Override
	public Stmt getAction() {
		return EcjUtils.wrap(getNested().action, this);
	}

	@Override
	public Expr getCondition() {
		return EcjUtils.wrap(getNested().condition, this);
	}

	@Override
	public boolean isPostfix() {
		return false;
	}
	
	public static class DoWhile extends NestedNode<DoStatement>  implements WhileStmt {

		public DoWhile(DoStatement nested, Node p) {
			super(nested, p);
		}

		@Override
		public Stmt getAction() {
			return EcjUtils.wrap(getNested().action, this);
		}

		@Override
		public Expr getCondition() {
			return EcjUtils.wrap(getNested().condition, this);
		}

		@Override
		public boolean isPostfix() {
			return false;
		}
	}
}
