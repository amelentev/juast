/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import juast.CaseNode;
import juast.Expr;
import juast.Node;
import juast.Stmt;
import juast.common.NestedNode;

import org.eclipse.jdt.internal.compiler.ast.CaseStatement;
import org.eclipse.jdt.internal.compiler.ast.Statement;
import org.eclipse.jdt.internal.compiler.ast.SwitchStatement;

public class EWSwitchStmt extends NestedNode<SwitchStatement> implements
		juast.SwitchStmt {

	public EWSwitchStmt(SwitchStatement nested, Node p) {
		super(nested, p);
	}

	@Override
	public List<CaseNode> getCases() {
		List<CaseNode> res = new ArrayList<CaseNode>();
		EWCaseNode lastcase = null;
		List<Stmt> stmts = new ArrayList<Stmt>();
		for (Statement s : getNested().statements) {
			if (s instanceof CaseStatement) {
				stmts = new ArrayList<Stmt>();
				lastcase = new EWCaseNode((CaseStatement) s, this, stmts);
				res.add(lastcase);
			} else {
				stmts.add(EcjUtils.wrap(s, lastcase));
			}
		}
		return res;
	}

	@Override
	public Expr getExpression() {
		return EcjUtils.wrap(getNested().expression, this);
	}
	
	public static class EWCaseNode extends NestedNode<CaseStatement> implements CaseNode {
		private List<Stmt> stmts;
		
		public EWCaseNode(CaseStatement nested, Node p, List<Stmt> stmts) {
			super(nested, p);
			this.stmts = stmts;
		}

		@Override
		public Expr getCase() {
			return EcjUtils.wrap( getNested().constantExpression, this );
		}

		@Override
		public List<juast.Stmt> getStatements() {
			return Collections.unmodifiableList(stmts);
		}
	}
}
