/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.lang.model.type.TypeMirror;

import juast.Annotation;
import juast.Expr;
import juast.MethodInvExpr;
import juast.Node;
import juast.common.BaseUtils;

import org.eclipse.jdt.internal.compiler.ast.MessageSend;
import org.eclipse.jdt.internal.compiler.lookup.AnnotationBinding;
import org.eclipse.jdt.internal.compiler.lookup.AnnotationHolder;
import org.eclipse.jdt.internal.compiler.lookup.ReferenceBinding;

public class EWMethodInvExpr extends AEWExpr<MessageSend> implements MethodInvExpr {

	public EWMethodInvExpr(MessageSend nested, Node p) {
		super(nested, p);
	}

	@Override
	public List<Expr> getArgs() {
		return EcjUtils.transformE(getNested().arguments, this);
	}

	@Override
	public String getMethod() {
		return new String(getNested().selector);
	}

	@Override
	public Expr getReceiver() {
		return EcjUtils.wrap(getNested().receiver, this);
	}

	@Override
	public List<TypeMirror> getTypeArgs() {
		return transformT(getNested().typeArguments);
	}

	private AnnotationHolder getAnnotationHolder() {
		ReferenceBinding rb = (ReferenceBinding) getNested().actualReceiverType;
		return rb != null
			? rb.retrieveAnnotationHolder(getNested().binding, false) 
			: null;
	}

	public List<Annotation> getAnnotations() {
		AnnotationHolder ah = getAnnotationHolder();
		if (ah == null)
			return Collections.emptyList();
		AnnotationBinding[] list;
		try {
			Method m = AnnotationHolder.class.getDeclaredMethod("getAnnotations");
			m.setAccessible(true);
			list = (AnnotationBinding[]) m.invoke(ah);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return BaseUtils.transform(list, new fj.F<AnnotationBinding, Annotation>() {
			public Annotation f(AnnotationBinding a) {
				return new EWAnnotation.EWAnnotationBinding(a, EWMethodInvExpr.this);
			}
		});
	}

	@Override
	public List<List<Annotation>> getArgsAnnotations() {
		int argsSize = getArgs().size();
		AnnotationHolder ah = getAnnotationHolder();
		if (ah != null) {
			AnnotationBinding[][] list = ah.getParameterAnnotations();
			if (list != null) {
				List<List<Annotation>> res = new ArrayList<List<Annotation>>(list.length);
				for (AnnotationBinding[] p : list) {
					res.add( BaseUtils.transform(p, new fj.F<AnnotationBinding, Annotation>() {
						public Annotation f(AnnotationBinding a) {
							return new EWAnnotation.EWAnnotationBinding(a, EWMethodInvExpr.this);
						}
					}) );
				}
				while (res.size() < argsSize) // duplicate annotation for varArgs params
					res.add(res.get(res.size()-1));
				return res;
			}
		}
		List<List<Annotation>> res = new ArrayList<List<Annotation>>(getArgs().size());
		for (int i=0; i<argsSize; i++)
			res.add(Collections.<Annotation>emptyList());
		return res;
	}
}
