/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.util.HashMap;
import java.util.Map;

import juast.Expr;
import juast.Node;
import juast.common.NestedNode;

import org.eclipse.jdt.internal.compiler.ast.Annotation;
import org.eclipse.jdt.internal.compiler.ast.MemberValuePair;
import org.eclipse.jdt.internal.compiler.lookup.AnnotationBinding;
import org.eclipse.jdt.internal.compiler.lookup.ElementValuePair;

public class EWAnnotation extends NestedNode<Annotation> implements juast.Annotation {
	public EWAnnotation(Annotation nested, Node p) {
		super(nested, p);
	}

	@Override
	public String getTypeName() {
		if (getNested().resolvedType != null)
			return new String(getNested().resolvedType.readableName());
		return getNested().type.toString();
	}

	@Override
	public Map<String, Expr> getValues() {
		Map<String, Expr> res = new HashMap<String, Expr>();
		for (MemberValuePair mvp : getNested().memberValuePairs())
			res.put(new String(mvp.name), EcjUtils.wrap(mvp.value, this));
		return res;
	}

	public static class EWAnnotationBinding extends NestedNode<AnnotationBinding> implements juast.Annotation {

		public EWAnnotationBinding(AnnotationBinding nested, Node parent) {
			super(nested, parent);
		}

		@Override
		public String getTypeName() {
			return new String(getNested().getAnnotationType().readableName());
		}

		@Override
		public Map<String, Expr> getValues() {
			Map<String, Expr> res = new HashMap<String, Expr>();
			for (ElementValuePair e : getNested().getElementValuePairs()) {
				res.put(new String(e.getName()), null);
				// TODO: e.getValue()
			}
			return res;
		}
	}
}
