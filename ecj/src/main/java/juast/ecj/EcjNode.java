/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.lang.reflect.Field;
import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.type.TypeMirror;

import juast.Node;
import juast.NodeW;
import juast.common.BaseUtils;
import juast.common.NestedNode;
import juast.common.BasicTypeMirror;

import org.eclipse.jdt.internal.compiler.apt.dispatch.BaseProcessingEnvImpl;
import org.eclipse.jdt.internal.compiler.apt.model.Factory;
import org.eclipse.jdt.internal.compiler.ast.ASTNode;
import org.eclipse.jdt.internal.compiler.ast.Expression;
import org.eclipse.jdt.internal.compiler.ast.TypeReference;
import org.eclipse.jdt.internal.compiler.lookup.TypeBinding;

public class EcjNode<T extends ASTNode> extends NestedNode<T> implements NodeW {
	public EcjNode(T nested, Node parent) {
		super(nested, parent);
	}
	
	protected Factory getFactory() {
		BaseProcessingEnvImpl pe = (BaseProcessingEnvImpl) getContext().getInjector().getInstance(ProcessingEnvironment.class);
		return pe.getFactory();
	}

	@Override
	public void replace(Node oldChild, Node newChild) {
		ASTNode nested = (ASTNode) getNested(),
			oldc = (ASTNode) oldChild.getNested(),
			newc = (ASTNode) newChild.getNested();
		// TODO: support replace in lists
		for (Field f : nested.getClass().getFields()) {
			if (!ASTNode.class.isAssignableFrom(f.getType()))
				continue;
			try {
				if (f.get(nested) == oldc) {
					f.set(nested, newc);
					break;
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	protected TypeMirror transformT(Expression type) {
		if (type == null)
			return null;
		if (type.resolvedType == null)
			return new BasicTypeMirror(type);
		return transformT(type.resolvedType); 
	}
	protected TypeMirror transformT(TypeReference type) {
		if (type == null)
			return null;
		if (type.resolvedType == null)
			return new BasicTypeMirror(type);
		return transformT(type.resolvedType); 
	}
	protected TypeMirror transformT(TypeBinding type) {
		if (type == null)
			return null;
		return getFactory().newTypeMirror(type); 
	}
	protected List<TypeMirror> transformT(TypeReference[] types) {
		return BaseUtils.transform(types, new fj.F<TypeReference, TypeMirror>() {
			public TypeMirror f(TypeReference a) {
				return transformT(a);
			}
		});
	}
}
