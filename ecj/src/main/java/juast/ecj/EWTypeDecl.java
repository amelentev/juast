/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.lang.model.element.Modifier;
import javax.lang.model.type.TypeMirror;

import juast.Annotation;
import juast.BodyDecl;
import juast.Node;
import juast.common.NotImplementedException;

import org.eclipse.jdt.internal.compiler.ast.AbstractMethodDeclaration;
import org.eclipse.jdt.internal.compiler.ast.FieldDeclaration;
import org.eclipse.jdt.internal.compiler.ast.Initializer;
import org.eclipse.jdt.internal.compiler.ast.TypeDeclaration;

import fj.F;

public class EWTypeDecl extends EcjNode<TypeDeclaration> implements juast.TypeDecl {
	public EWTypeDecl(TypeDeclaration nested, Node p) {
		super(nested, p);
	}

	@Override
	public List<BodyDecl> getBody() {
		fj.data.List<BodyDecl> res = fj.data.List.nil();
		if (getNested().fields != null)
			res = res.append( fj.data.List.list(getNested().fields)
				.map(new F<FieldDeclaration, BodyDecl>() {
					public BodyDecl f(FieldDeclaration a) {
						if (a instanceof Initializer)
							return new EWMethodDecl.EWInitializer((Initializer) a, EWTypeDecl.this);
						return new EWVarDecl(a, EWTypeDecl.this);
					};
				}));
		if (getNested().methods != null)
			res = res.append( fj.data.List.list(getNested().methods)
				.map(new F<AbstractMethodDeclaration, BodyDecl>() {
					public BodyDecl f(AbstractMethodDeclaration a) {
						return new EWMethodDecl(a, EWTypeDecl.this);
					};
				}));
		if (getNested().memberTypes != null)
			res = res.append( fj.data.List.list(getNested().memberTypes)
				.map(new F<TypeDeclaration, BodyDecl>() {
					public BodyDecl f(TypeDeclaration a) {
						return new EWTypeDecl(a, EWTypeDecl.this);
					};
				}));
		return Collections.unmodifiableList( new ArrayList<BodyDecl>( res.toCollection() ) );
	}

	@Override
	public TypeMirror getExtends() {
		return transformT( getNested().superclass );
	}

	@Override
	public List<TypeMirror> getImplements() {
		return transformT(getNested().superInterfaces);
	}

	@Override
	public String getName() {
		return new String(getNested().name);
	}

	@Override
	public TypeKind getKind() {
		switch (TypeDeclaration.kind(getNested().modifiers)) {
		case TypeDeclaration.CLASS_DECL:
			return TypeKind.Class;
		case TypeDeclaration.INTERFACE_DECL:
			return TypeKind.Interface;
		case TypeDeclaration.ENUM_DECL:
			return TypeKind.Enum;
		case TypeDeclaration.ANNOTATION_TYPE_DECL:
			return TypeKind.AnnotationType;
		default:
			throw new NotImplementedException();
		}
	}

	@Override
	public List<juast.TypeParam> getTypeParameters() {
		return EcjUtils.transformTP(getNested().typeParameters, this);
	}

	@Override
	public List<Annotation> getAnnotations() {
		return EcjUtils.transformA(getNested().annotations, this);
	}

	@Override
	public Set<Modifier> getModifiers() {
		return EcjUtils.getModifiers(getNested().modifiers);
	}
}
