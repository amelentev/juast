/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import static juast.common.BaseUtils.transform;

import java.util.Collections;
import java.util.List;

import juast.Annotation;
import juast.Import;
import juast.common.BaseUtils;
import juast.common.Context;
import juast.common.NestedNode;

import org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration;
import org.eclipse.jdt.internal.compiler.ast.ImportReference;
import org.eclipse.jdt.internal.compiler.ast.TypeDeclaration;

import fj.F;

public class EWCompilationUnit extends NestedNode<CompilationUnitDeclaration> implements juast.CompilationUnit {

	public EWCompilationUnit(CompilationUnitDeclaration nested, Context context) {
		super(nested, null);
		this.context = context;
	}

	@Override
	public List<Import> getImports() {
		return transform( getNested().imports, new fj.F<ImportReference, Import>() {
			public Import f(ImportReference a) {
				return new EWImport(a, EWCompilationUnit.this);
			}
		});
	}

	@Override
	public List<juast.TypeDecl> getTypes() {
		return BaseUtils.transform(getNested().types, new F<TypeDeclaration, juast.TypeDecl>() {
			public juast.TypeDecl f(TypeDeclaration a) {
				return new EWTypeDecl(a, EWCompilationUnit.this);
			}
		});
	}

	@Override
	public List<Annotation> getAnnotations() {
		return getNested().currentPackage!=null ? EcjUtils.transformA(getNested().currentPackage.annotations, this) : Collections.<Annotation>emptyList();
	}

	@Override
	public String getPackageName() {
		return getNested().currentPackage!=null ? EcjUtils.getPkg( getNested().currentPackage.getImportName() ) : "";
	}
	
	@Override
	public String getFileName() {
		return new String(getNested().getFileName());
	}
}
