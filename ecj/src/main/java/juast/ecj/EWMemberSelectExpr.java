/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.util.Collections;
import java.util.List;

import juast.Annotation;
import juast.Expr;
import juast.IdExpr;
import juast.MemberSelectExpr;
import juast.Node;

import org.eclipse.jdt.internal.compiler.ast.ClassLiteralAccess;
import org.eclipse.jdt.internal.compiler.ast.FieldReference;
import org.eclipse.jdt.internal.compiler.ast.MessageSend;
import org.eclipse.jdt.internal.compiler.ast.QualifiedNameReference;

public class EWMemberSelectExpr extends AEWExpr<MessageSend> implements MemberSelectExpr {

	public EWMemberSelectExpr(MessageSend nested, Node p) {
		super(nested, p);
	}

	@Override
	public Expr getExpr() {
		return EcjUtils.wrap(getNested().receiver, this);
	}

	@Override
	public String getId() {
		return new String(getNested().selector);
	}
	
	public static class QNE extends AEWExpr<QualifiedNameReference> implements MemberSelectExpr {
		int ind;
		
		public QNE(QualifiedNameReference qn, Node p) {
			this(qn, qn.tokens.length-1, p);
		}

		public QNE(QualifiedNameReference qn, int ind, Node p) {
			super(qn, p);
			this.ind = ind;
		}

		@Override
		public Expr getExpr() {
			if (ind > 1)
				return new QNE(getNested(), ind-1, this);
			return new QNIdentifierExpression(getNested(), this);
		}

		static class QNIdentifierExpression extends AEWExpr<QualifiedNameReference> implements IdExpr {
			public QNIdentifierExpression(QualifiedNameReference qnr, Node parent) {
				super(qnr, parent);
			}

			@Override
			public String getId() {
				return new String(getNested().tokens[0]);
			}

			@Override
			public List<Annotation> getAnnotations() {
				return Collections.emptyList();
			}
		}

		@Override
		public String getId() {
			return new String(getNested().tokens[ind]);
		}
	}
	
	public static class ClassLiteral extends AEWExpr<ClassLiteralAccess> implements MemberSelectExpr {
		public ClassLiteral(ClassLiteralAccess nested, Node p) {
			super(nested, p);
		}

		@Override
		public Expr getExpr() {
			return new EWIdExpr.ClassIdentifierExpression(getNested().type, this);
		}

		@Override
		public String getId() {
			return "class";
		}
	}
	
	public static class Field extends AEWExpr<FieldReference> implements MemberSelectExpr {
		public Field(FieldReference nested, Node p) {
			super(nested, p);
		}

		@Override
		public Expr getExpr() {
			return EcjUtils.wrap(getNested().receiver, this);
		}

		@Override
		public String getId() {
			return new String(getNested().token);
		}
	}
}
