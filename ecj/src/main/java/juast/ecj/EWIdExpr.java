/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.util.Collections;
import java.util.List;

import juast.Annotation;
import juast.IdExpr;
import juast.Node;

import org.eclipse.jdt.internal.compiler.ast.SingleNameReference;
import org.eclipse.jdt.internal.compiler.ast.ThisReference;
import org.eclipse.jdt.internal.compiler.ast.TypeReference;
import org.eclipse.jdt.internal.compiler.lookup.Binding;
import org.eclipse.jdt.internal.compiler.lookup.LocalVariableBinding;

public class EWIdExpr extends AEWExpr<SingleNameReference> implements IdExpr {

	public EWIdExpr(SingleNameReference nested, Node p) {
		super(nested, p);
	}

	@Override
	public String getId() {
		return getNested().toString();
	}

	public List<Annotation> getAnnotations() {
		Binding b = getNested().binding;
		if (b instanceof LocalVariableBinding)
			return new EWVarDecl(((LocalVariableBinding) b).declaration, this).getAnnotations();
		// TODO: FieldBinding
		return Collections.emptyList();
	}

	public static class This extends AEWExpr<ThisReference> implements IdExpr {
		public This(ThisReference nested, Node p) {
			super(nested, p);
		}

		@Override
		public String getId() {
			return getNested().toString();
		}

		@Override
		public List<Annotation> getAnnotations() {
			// TODO DO!
			return Collections.emptyList();
		}
	}
	
	public static class ClassIdentifierExpression extends AEWExpr<TypeReference> implements IdExpr {
		public ClassIdentifierExpression(TypeReference nested, Node p) {
			super(nested, p);
		}

		@Override
		public String getId() {
			return getNested().toString();
		}

		@Override
		public List<Annotation> getAnnotations() {
			// TODO DO
			return Collections.emptyList();
		}
	}
}
