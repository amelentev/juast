/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import juast.CompilationUnit;
import juast.JuastParcer;
import juast.common.Context;

import org.eclipse.jdt.internal.compiler.CompilationResult;
import org.eclipse.jdt.internal.compiler.IErrorHandlingPolicy;
import org.eclipse.jdt.internal.compiler.IProblemFactory;
import org.eclipse.jdt.internal.compiler.ast.CompilationUnitDeclaration;
import org.eclipse.jdt.internal.compiler.classfmt.ClassFileConstants;
import org.eclipse.jdt.internal.compiler.env.ICompilationUnit;
import org.eclipse.jdt.internal.compiler.impl.CompilerOptions;
import org.eclipse.jdt.internal.compiler.parser.Parser;
import org.eclipse.jdt.internal.compiler.problem.DefaultProblemFactory;
import org.eclipse.jdt.internal.compiler.problem.ProblemReporter;

import com.google.inject.Inject;

public class EcjJuastParcer implements JuastParcer {
	private @Inject Context context;

	final IErrorHandlingPolicy errorHandlingPolicy = new IErrorHandlingPolicy() {
		@Override
		public boolean stopOnFirstError() {
			return false;
		}
		@Override
		public boolean proceedOnErrors() {
			return false;
		}
	};
	@Override
	public CompilationUnit parce(final String text) {
		CompilerOptions compilerOptions = new CompilerOptions();
		compilerOptions.sourceLevel = ClassFileConstants.JDK1_6; 
		IProblemFactory problemFactory = new DefaultProblemFactory();
		ProblemReporter problemReporter = new ProblemReporter(errorHandlingPolicy, compilerOptions, problemFactory);
		Parser parser = new Parser(problemReporter, true);
		
		ICompilationUnit compilationUnit = new ICompilationUnit() {
			public char[] getContents() {
				return text.toCharArray();
			}
			public char[] getMainTypeName() {
				return "Test".toCharArray();
			}
			public char[][] getPackageName() {
				return new char[][]{"test".toCharArray()};
			}
			public char[] getFileName() {
				return "Test.java".toCharArray();
			}
		};
		CompilationResult result = new CompilationResult("Test.java".toCharArray(), 0, 0, 0);
		CompilationUnitDeclaration res = parser.parse(compilationUnit, result);
		return new EWCompilationUnit(res, context);
	}
}
