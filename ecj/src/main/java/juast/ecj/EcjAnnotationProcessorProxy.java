/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedOptions;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;

/**
 * Proxy to {@link EcjAnnotationProcessor}.
 * Because javac can't create {@link EcjAnnotationProcessor} dirrectly.
 */
@SupportedAnnotationTypes("*")
@SupportedSourceVersion(SourceVersion.RELEASE_7)
@SupportedOptions("juast.process")
public class EcjAnnotationProcessorProxy extends AbstractProcessor {
	private static boolean isSupported(ProcessingEnvironment processingEnv) {
		if (processingEnv == null)
			return false;
		try {
			Class<?> baseClass = Class.forName("org.eclipse.jdt.internal.compiler.apt.dispatch.BaseProcessingEnvImpl");
			return baseClass.isAssignableFrom( processingEnv.getClass() );
		} catch (ClassNotFoundException e) {
			return false;
		}
	}
	EcjAnnotationProcessor processor;

	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		if (!isSupported(processingEnv))
			return;
		processor = new EcjAnnotationProcessor();
		processor.init(processingEnv);
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		if (processor!=null)
			return processor.process(annotations, roundEnv);
		return false;
	}
}
