/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import juast.CompoundAssignExpr;
import juast.Expr;
import juast.Node;

import org.eclipse.jdt.internal.compiler.ast.CompoundAssignment;

public class EWCompoundAssignExpr extends AEWExpr<CompoundAssignment> implements CompoundAssignExpr {

	public EWCompoundAssignExpr(CompoundAssignment nested, Node p) {
		super(nested, p);
	}

	@Override
	public Expr getExpression() {
		return EcjUtils.wrap( getNested().expression, this );
	}

	@Override
	public String getOperator() {
		return getNested().operatorToString();
	}

	@Override
	public Expr getVariable() {
		return EcjUtils.wrap( getNested().lhs, this );
	}
}
