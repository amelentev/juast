/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import juast.Node;
import juast.common.BaseUtils;
import juast.common.NestedNode;

import java.util.List;

import org.eclipse.jdt.internal.compiler.ast.TypeParameter;
import org.eclipse.jdt.internal.compiler.ast.TypeReference;

import fj.F;

public class EWTypeParam extends NestedNode<TypeParameter> implements juast.TypeParam {
	public EWTypeParam(TypeParameter nested, Node p) {
		super(nested, p);
	}

	@Override
	public List<String> getBounds() {
		return BaseUtils.transform( getNested().bounds, new F<TypeReference, String>() {
			public String f(TypeReference a) {
				return a.toString();
			}
		});
	}

	@Override
	public String getName() {
		return new String(getNested().name);
	}
}
