/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.util.List;

import javax.lang.model.type.TypeMirror;

import juast.Expr;
import juast.JuastTreeMaker;
import juast.MemberSelectExpr;
import juast.MethodInvExpr;

import org.eclipse.jdt.internal.compiler.ast.Expression;
import org.eclipse.jdt.internal.compiler.ast.MessageSend;

public class EcjJuastTreeMaker implements JuastTreeMaker {
	Expression from(Expr e) {
		return (Expression) e.getNested();
	}
	Expression[] from(List<Expr> le) {
		if (le == null)
			return null;
		Expression[] res = new Expression[le.size()];
		for (int i=0; i<res.length; i++)
			res[i] = from(le.get(i));
		return res;
	}
	
	@Override
	public MethodInvExpr methodInvExpr(List<TypeMirror> typeArgs, Expr method, List<Expr> args) {
		EWMemberSelectExpr mse = (EWMemberSelectExpr) method;
		MessageSend ms = mse.getNested();
		// TODO: ms.typeArguments
		ms.arguments = from(args);
		return new EWMethodInvExpr(ms, null);
	}

	// TODO: support all types
	@Override
	public MemberSelectExpr memberSelectExpr(Expr expr, String id) {
		MessageSend nested = new MessageSend();
		nested.selector = id.toCharArray();
		nested.receiver = from(expr);
		return new EWMemberSelectExpr(nested, null);
	}
}
