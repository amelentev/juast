/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.util.List;

import juast.Node;
import juast.Stmt;
import juast.common.NestedNode;

import org.eclipse.jdt.internal.compiler.ast.AbstractMethodDeclaration;
import org.eclipse.jdt.internal.compiler.ast.Block;

public class EWBlock extends NestedNode<Block> implements juast.Block {
	public EWBlock(Block nested, Node p) {
		super(nested, p);
	}
	
	@Override
	public List<Stmt> getStmts() {
		return EcjUtils.transformS( getNested()==null ? null : getNested().statements, this );
	}

	public static class MethodBlock extends NestedNode<AbstractMethodDeclaration> implements juast.Block {
		public MethodBlock(AbstractMethodDeclaration nested, Node p) {
			super(nested, p);
		}
	
		@Override
		public List<juast.Stmt> getStmts() {
			return EcjUtils.transformS(getNested().statements, this);
		}
	}
}
