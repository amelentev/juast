/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.util.List;
import java.util.Set;

import javax.lang.model.element.Modifier;
import javax.lang.model.type.TypeMirror;

import juast.Annotation;
import juast.MethodParam;
import juast.Node;

import org.eclipse.jdt.internal.compiler.ast.Argument;

public class EWMethodParam extends EcjNode<Argument> implements MethodParam {
	public EWMethodParam(Argument nested, Node p) {
		super(nested, p);
	}

	@Override
	public Set<Modifier> getModifiers() {
		return EcjUtils.getModifiers(getNested().modifiers);
	}

	@Override
	public String getName() {
		return new String(getNested().name);
	}

	@Override
	public TypeMirror getType() {
		return transformT(getNested().type);
	}

	@Override
	public List<Annotation> getAnnotations() {
		return EcjUtils.transformA(getNested().annotations, this);
	}
}
