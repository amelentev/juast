/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.ecj;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.lang.model.element.Modifier;
import javax.lang.model.type.TypeMirror;

import juast.Annotation;
import juast.Block;
import juast.Expr;
import juast.MethodParam;
import juast.Node;
import juast.TypeParam;
import juast.common.BaseUtils;
import juast.common.NestedNode;
import juast.common.NotImplementedException;

import org.eclipse.jdt.internal.compiler.ast.AbstractMethodDeclaration;
import org.eclipse.jdt.internal.compiler.ast.AnnotationMethodDeclaration;
import org.eclipse.jdt.internal.compiler.ast.Argument;
import org.eclipse.jdt.internal.compiler.ast.Clinit;
import org.eclipse.jdt.internal.compiler.ast.ConstructorDeclaration;
import org.eclipse.jdt.internal.compiler.ast.Initializer;
import org.eclipse.jdt.internal.compiler.ast.MethodDeclaration;

import fj.F;

public class EWMethodDecl extends EcjNode<AbstractMethodDeclaration> implements juast.MethodDecl {
	public EWMethodDecl(AbstractMethodDeclaration nested, Node p) {
		super(nested, p);
	}

	@Override
	public Block getBody() {
		return new EWBlock.MethodBlock(getNested(), this);
	}

	@Override
	public Expr getDefaultValue() {
		if (getNested() instanceof AnnotationMethodDeclaration)
			return EcjUtils.wrap(((AnnotationMethodDeclaration) getNested()).defaultValue, this);
		return null;
	}

	@Override
	public String getName() {
		return new String(getNested().selector);
	}

	@Override
	public List<MethodParam> getParameters() {
		return BaseUtils.transform(getNested().arguments, new F<Argument, MethodParam>() {
			public MethodParam f(Argument a) {
				return new EWMethodParam(a, EWMethodDecl.this);
			}
		});
	}

	@Override
	public TypeMirror getReturnType() {
		if (getNested() instanceof MethodDeclaration)
			return transformT(((MethodDeclaration)getNested()).returnType);
		if (getNested() instanceof ConstructorDeclaration || getNested() instanceof Clinit)
			return null;
		throw new NotImplementedException();
	}

	@Override
	public List<TypeMirror> getThrows() {
		return transformT(getNested().thrownExceptions);
	}

	@Override
	public List<TypeParam> getTypeParameters() {
		return EcjUtils.transformTP(getNested().typeParameters(), this);
	}

	@Override
	public List<Annotation> getAnnotations() {
		return EcjUtils.transformA(getNested().annotations, this);
	}

	@Override
	public Set<Modifier> getModifiers() {
		return EcjUtils.getModifiers(getNested().modifiers);
	}

	@Override
	public MethodKind getKind() {
		if (getNested() instanceof ConstructorDeclaration || getNested() instanceof Clinit)
			return MethodKind.Constructor;
		if (getNested() instanceof AnnotationMethodDeclaration)
			return MethodKind.AnnotationMethod;
		return MethodKind.Method;
	}
	
	public static class EWInitializer extends NestedNode<Initializer> implements juast.MethodDecl {
		public EWInitializer(Initializer nested, Node p) {
			super(nested, p);
		}

		@Override
		public Block getBody() {
			return new EWBlock(getNested().block, this);
		}

		@Override
		public Expr getDefaultValue() {
			return null;
		}

		@Override
		public MethodKind getKind() {
			return MethodKind.Initializer;
		}

		@Override
		public String getName() {
			return null;
		}

		@Override
		public List<MethodParam> getParameters() {
			return Collections.emptyList();
		}

		@Override
		public TypeMirror getReturnType() {
			return null;
		}

		@Override
		public List<TypeMirror> getThrows() {
			return Collections.emptyList();
		}

		@Override
		public List<TypeParam> getTypeParameters() {
			return Collections.emptyList();
		}

		@Override
		public List<Annotation> getAnnotations() {
			return EcjUtils.transformA(getNested().annotations, this);
		}

		@Override
		public Set<Modifier> getModifiers() {
			return EcjUtils.getModifiers(getNested().modifiers);
		}
	}
}
