/**
 * Copyright (C) 2010  Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

import java.math.BigInteger;

public class OperatorTest {
	@SuppressWarnings("all")
	public static void main(String[] args) {
		BigInteger a = BigInteger.ONE,
			b = BigInteger.TEN,
			c = BigInteger.valueOf(2),
			d = a + b*c;
		System.out.println("\n1 + 10*2 = " + d);
	}
}
