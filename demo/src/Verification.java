// Demo of Verificators
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;

import juast.annotations.Safe;
import juast.annotations.Unsafe;

public class Verification {
	class CloseTest {
		void goodMethod() throws Exception {
			BufferedReader br = new BufferedReader(new FileReader(new File("pom.xml")));
			try {
				System.out.println( br.readLine() );
			} finally {
				br.close();
			}
		}
	
		void badMethod() throws Exception {
			// warning: Closable not properly closed
			BufferedReader br = new BufferedReader(new FileReader(new File("pom.xml")));
			System.out.println( br.readLine() );
			br.close();
		}
	}
	class CharsetTest {
		Object o;
		void charSetCode() throws Exception {
			// good
			o = new PrintWriter("somefile", "UTF8");
			o = new OutputStreamWriter(null, "UTF8");
			o = new OutputStreamWriter(null, Charset.defaultCharset());
			o = new InputStreamReader(null, "UTF8");
			// bad
			o = new PrintWriter("somefile");
			o = new OutputStreamWriter(null);
			o = new InputStreamReader(null);
		}
	}
	class SafeTest {
		void doSome(@Safe String params) {
		}
		@Safe String sfencode(@Unsafe String param) {
			return "'"+param.replaceAll("\"", "\\\"")+"'";
		}
		void run() {
			@Unsafe String userParams = "user \" params \"; drop table sometable";
			doSome(userParams); // error
			@Safe String encodedUserParams = sfencode(userParams);
			doSome(encodedUserParams);
			encodedUserParams = userParams; // error
			@Safe String s = userParams; // error
			doSome(s);
		}
	}
}
