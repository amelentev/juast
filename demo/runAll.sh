echo "Run verificators"
javac -cp ../package/target/juast*.jar src/Verification.java -d target
ecj -6 -cp ../package/target/juast*.jar src/Verification.java -d target

echo "Run VCSProcessor"
javac -cp ../package/target/juast*.jar src/VCSTest.java -d target -sourcepath src/ -Avcs=git -Ajuast.process=juast.processors.VCSProcessor
ecj -6 -cp ../package/target/juast*.jar src/VCSTest.java -d target -sourcepath src/ -Avcs=git -Ajuast.process=juast.processors.VCSProcessor

echo "Run operator overloading"
javac -cp ../package/target/juast*.jar src/OperatorTest.java -d target
java -cp target OperatorTest
ecj -6 -cp ../package/target/juast*.jar src/OperatorTest.java -d target
java -cp target OperatorTest
