/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.processors;

import juast.Warnings;

public class SystemOutPrintlnProcessorTest extends AProcessorTest {
	void someMethod() {
		System.out.println(" hi "); // error
		Warnings.suppress();
		System.out.println(" hi ");
	}
	@SuppressWarnings("all")
	void suppressBlockTest() throws Exception {
		System.out.println(" hi ");
	}

	public SystemOutPrintlnProcessorTest() {
		super(SystemOutPrintlnProcessor.class);
		addErrors(SystemOutPrintlnProcessor.errorMessage, 24);
	}
}
