/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.processors;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.Charset;

import juast.Warnings;

public class EnsureCharSetTest extends AProcessorTest {
	Object o;
	void charSetCode() throws Exception {
		// good
		o = new PrintWriter("somefile", "UTF8");
		o = new OutputStreamWriter(null, "UTF8");
		o = new OutputStreamWriter(null, Charset.defaultCharset());
		o = new InputStreamReader(null, "UTF8");
		// bad
		o = new PrintWriter("somefile");
		o = new OutputStreamWriter(null);
		o = new InputStreamReader(null);
		// suppress bad
		Warnings.suppress();
		o = new PrintWriter("somefile");
		@SuppressWarnings("all")
		Writer o1 = new OutputStreamWriter(null);
		o1.close();
	}
	@SuppressWarnings("all")
	void suppressBlockTest() throws Exception {
		o = new PrintWriter("somefile");
	}

	public EnsureCharSetTest() {
		super(EnsureCharSet.class);
		addErrors(EnsureCharSet.errorMessage, 37, 38, 39);
	}
}
