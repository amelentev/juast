/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.processors;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class VCSProcessorTest extends AProcessorTest {
	public VCSProcessorTest() {
		super(VCSProcessor.class);
		setSourcePath("src/test/sources");
		setJavaFile("VCSTest.java");
		addOptions("-Avcs=hg");
		addErrors(VCSProcessor.errorMessage, 1);
	}

	@BeforeClass
	static public void setup() throws IOException {
		File dir = new File("src/test/sources/vcs");
		dir.mkdir();
		FileWriter fw = new FileWriter(new File(dir, "Import.java"));
		try {
			IOUtils.write("package vcs; public class Import {}", fw);
		} finally {
			fw.close();
		}
	}
	@AfterClass
	static public void cleanup() {
		new File("src/test/sources/vcs/Import.java").delete();
		new File("src/test/sources/vcs/").delete();
	}
}
