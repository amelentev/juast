/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.processors;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import juast.Warnings;

public class EnsureCloseProcessorTest extends AProcessorTest {
	void goodMethod() throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(new File("pom.xml")));
		try {
			System.out.println( br.readLine() );
		} finally {
			br.close();
		}
	}

	void badMethod() throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(new File("pom.xml")));
		System.out.println( br.readLine() );
		br.close();
	}

	void supressWarnings() throws Exception {
		@SuppressWarnings("all")
		BufferedReader br = new BufferedReader(new FileReader(new File("pom.xml")));
		System.out.println( br.readLine() );
		br.close();
	}
	void supressWarnings14() throws Exception {
		Warnings.suppress();
		BufferedReader br = new BufferedReader(new FileReader(new File("pom.xml")));
		System.out.println( br.readLine() );
		br.close();
	}
	@SuppressWarnings("all")
	void suppressBlockTest() throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(new File("pom.xml")));
		System.out.println( br.readLine() );
		br.close();
	}

	public EnsureCloseProcessorTest() {
		super(EnsureCloseProcessor.class);
		addErrors(EnsureCloseProcessor.errorMessage, 37);
	}
}
