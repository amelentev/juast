/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.processors;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import juast.ecj.EcjAnnotationProcessorProxy;
import juast.javac.JavacAnnotationProcessorProxy;
import juast.process.JuastProcessor;

import org.eclipse.jdt.internal.compiler.tool.EclipseCompiler;
import org.junit.Assert;
import org.junit.Test;

public abstract class AProcessorTest {
	private String javaFile;
	private Class<?> processor;
	private String sourcepath = "src/test/java";
	private List<String> opts = new ArrayList<String>();
	private Map<Long, Set<String>> expectedErrors = new HashMap<Long, Set<String>>();

	public AProcessorTest(Class<? extends JuastProcessor> processor) {
		this.processor = processor;
		this.javaFile = this.getClass().getName().replace(".", "/") + ".java";
	}
	public AProcessorTest setSourcePath(String sp) {
		this.sourcepath = sp;
		return this;
	}
	public AProcessorTest setJavaFile(String file) {
		this.javaFile = file;
		return this;
	}
	public AProcessorTest addOptions(String... opts) {
		this.opts.addAll(Arrays.asList(opts));
		return this;
	}
	public AProcessorTest addErrors(String msg, long... lines) {
		for (long l : lines) {
			Set<String> s = expectedErrors.get(l);
			if (s == null) {
				s = new HashSet<String>();
				expectedErrors.put(l, s);
			}
			s.add(msg);
		}
		return this;
	}

	@Test
	public void testJavac() {
		check(run(ToolProvider.getSystemJavaCompiler()));
	}
	@Test
	public void testEcj() {
		check(run(new EclipseCompiler()));
	}
	public List<Diagnostic<? extends JavaFileObject>> run(JavaCompiler compiler) {
		DiagnosticCollector<JavaFileObject> listener = new DiagnosticCollector<JavaFileObject>();
		StandardJavaFileManager fm = compiler.getStandardFileManager(listener, Locale.getDefault(), Charset.defaultCharset());
		File srcdir = new File(this.sourcepath);
		File destdir = new File("target/test");
		destdir.mkdir();
		Writer out = new StringWriter();
		List<String> opts = new ArrayList<String>(this.opts);
		opts.addAll(Arrays.asList("-source", "1.6",
				"-sourcepath", srcdir.toString(),
				"-d", destdir.toString(),
				"-Ajuast.process="+processor.getName()));
		CompilationTask task = compiler.getTask(out, null, listener, opts,
				null, fm.getJavaFileObjects(new File(srcdir, javaFile))); 
		task.setProcessors(Arrays.asList(new JavacAnnotationProcessorProxy(), new EcjAnnotationProcessorProxy()));
		boolean res = task.call();
		if (!res) {
			System.err.println(out.toString());
			throw new AssertionError("Compilation failed");
		}
		return listener.getDiagnostics();
	}

	Set<String> ignoredKeys = new HashSet<String>(Arrays.asList(
			null, // XXX: Eclipse 3.7 messager
			"compiler.note.proc.messager", 
			"compiler.warn.proc.use.proc.or.implicit",
			"compiler.warn.proc.unmatched.processor.options",
			"compiler.warn.source.no.bootclasspath"));

	public void check(List<Diagnostic<? extends JavaFileObject>> res) {
		Map<Long, Set<String>> expected = new HashMap<Long, Set<String>>(this.expectedErrors); // copy errors
		for (Diagnostic<? extends JavaFileObject> d : res) {
			if (ignoredKeys.contains(d.getCode()))
				continue; // ignore "juast: found Sun Java compiler" and other notes
			long line = d.getLineNumber();
			String msg = d.getMessage(Locale.getDefault());
			
			Assert.assertTrue("Unexpected error line: "+msg, expected.containsKey(line));
			Set<String> msgs = expected.get(line);
			boolean found = false;
			for (String s : msgs)
				if (msg.contains(s)) {
					msgs.remove(s);
					found = true;
					break;
				}
			Assert.assertTrue("Unexpected error msg: "+msg, found);
			expected.get(line).remove(msg);
			if (expected.get(line).size() == 0)
				expected.remove(line);
		}
		Assert.assertArrayEquals("Missing errors", new Object[0], expected.values().toArray());
	}
}
