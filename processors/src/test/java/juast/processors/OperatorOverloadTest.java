/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.processors;

import java.io.File;
import java.io.IOException;
import java.util.ServiceLoader;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import juast.process.JuastProcessor;

import org.apache.commons.io.FileUtils;
import org.eclipse.jdt.internal.compiler.tool.EclipseCompiler;
import org.junit.Test;
import static org.junit.Assert.*;

public class OperatorOverloadTest {
	String compiledFile = OperatorOverloadTest.class.getSimpleName();
	String commonOpts = "src/test/sources/"+compiledFile+".java"
		+" -source 1.6 -Ajuast.process="+OperatorOverload.class.getName();

	@Test
	public void testJavac() throws IOException {
		File dir = new File("target/javac/");
		FileUtils.deleteDirectory(dir);
		dir.mkdirs();
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		compiler.run(null, null, null, 
				(commonOpts + " -d target/javac").split(" ") );
		assertTrue(new File(dir, compiledFile+".class").exists());
	}

	@Test
	public void testEcj() throws IOException {
		File dir = new File("target/ecj/");
		FileUtils.deleteDirectory(dir);
		dir.mkdirs();
		JavaCompiler compiler = new EclipseCompiler();
		compiler.run(System.in, System.out, System.err, 
				(commonOpts + " -noExit -d target/ecj ").split(" ") );
		assertTrue(new File(dir, compiledFile+".class").exists());
	}

	@Test
	public void testRegistered() {
		int c = 0;
		for (JuastProcessor jp : ServiceLoader.load(JuastProcessor.class)) {
			if (jp instanceof OperatorOverload)
				c ++;
		}
		assertEquals(1, c);
	}
}
