/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.processors;

import juast.annotations.Safe;
import juast.annotations.Unsafe;

public class SafeTest extends AProcessorTest {
	void doSome(@Safe String params) {
	}
	@Safe String sfencode(@Unsafe String param) {
		return "'"+param.replaceAll("\"", "\\\"")+"'";
	}
	void run() {
		@Unsafe String userParams = "user \" params \"; drop table sometable";
		doSome(userParams); // error
		@Safe String encodedUserParams = sfencode(userParams);
		doSome(encodedUserParams);
		encodedUserParams = userParams; // error
		@Safe String s = userParams; // error
		doSome(s);
	}

	public SafeTest() {
		super(SafeProcessor.class);
		addErrors(SafeProcessor.errMethodReqSafe, 31);
		addErrors(SafeProcessor.errCannotAssign, 34, 35);
	}
}
