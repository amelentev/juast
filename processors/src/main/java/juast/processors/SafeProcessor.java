/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.processors;

import java.util.List;

import javax.annotation.processing.ProcessingEnvironment;

import juast.Annotable;
import juast.Annotation;
import juast.AssignExpr;
import juast.CompilationUnit;
import juast.Expr;
import juast.IdExpr;
import juast.MethodInvExpr;
import juast.Node;
import juast.ProblemReporter;
import juast.VarDecl;
import juast.annotations.Safe;
import juast.process.AJuastProcessor;

import com.google.inject.Inject;

public class SafeProcessor extends AJuastProcessor {
	@Inject private ProblemReporter pr;
	@Inject private ProcessingEnvironment pe;

	public static String errCannotAssign = "Cannot assign @Unsafe to @Safe";
	public static String errMethodReqSafe = "Method requires @Safe parameter";

	@Override
	public void afterAnalyse(final CompilationUnit unit) {
		unit.accept(new SuppressWarningsVisitor() {
			@Override
			public boolean enter(VarDecl node) {
				if (isMarkedSafe(node) && !isMarkedSafe(node.getInit()))
					pr.warning(unit, node, errCannotAssign);
				return true;
			}
			@Override
			public boolean enter(AssignExpr node) {
				if (isMarkedSafe(node.getVar()) && !isMarkedSafe(node.getExpr()))
					pr.warning(unit, node, errCannotAssign);
				return true;
			}
			@Override
			public boolean enter(MethodInvExpr node) {
				List<Expr> args = node.getArgs();
				List<List<Annotation>> argsAnns = node.getArgsAnnotations();
				for (int i=0; i < args.size(); i++) {
					if (AnnotationUtils.containName(argsAnns.get(i), Safe.class.getName()) 
							&& !isMarkedSafe(args.get(i)))
						pr.warning(unit, args.get(i), errMethodReqSafe);
				}
				return true;
			}
		});
	}

	boolean isPrefixBasedAnnotations() {
		return pe.getOptions().containsKey("safe.prefix");
	}
	final String safePrefix = "sf";
	final String unsafePrefix = "us";

	boolean isMarkedSafe(Node e) {
		if (e instanceof Annotable)
			for (Annotation a : ((Annotable) e).getAnnotations())
				if (a.getTypeName().equals(Safe.class.getName()))
					return true;
		if (isPrefixBasedAnnotations()) {
			String s = "";
			if (e instanceof IdExpr)
				s = ((IdExpr) e).getId();
			else if (e instanceof VarDecl)
				s = ((VarDecl) e).getVar();
			else if (e instanceof MethodInvExpr)
				s = ((MethodInvExpr) e).getMethod();
			return s.startsWith(safePrefix);
		}
		return false;
	}
}
