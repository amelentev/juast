/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.processors;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;

import juast.CompilationUnit;
import juast.NewClassExpr;
import juast.ProblemReporter;
import juast.process.AJuastProcessor;

import com.google.inject.Inject;

public class EnsureCharSet extends AJuastProcessor {
	@Inject private ProblemReporter pr;
	public static String errorMessage = "CharSet was not specified";

	@Override
	public void afterAnalyse(final CompilationUnit unit) {
		final Set<String> classes = new HashSet<String>(Arrays.asList(
				"java.io.PrintWriter", "java.io.OutputStreamWriter", "java.io.InputStreamReader"));
		unit.accept(new SuppressWarningsVisitor() {
			@Override
			public boolean enter(NewClassExpr node) {
				DeclaredType dt = (DeclaredType) node.getResolvedType();
				TypeElement te = (TypeElement) dt.asElement();
				if (classes.contains(te.getQualifiedName().toString())) {
					if (1 == node.getArguments().size()) {
						pr.warning(unit, node, errorMessage);
					}
				}
				return super.enter(node);
			}
		});
	}
}
