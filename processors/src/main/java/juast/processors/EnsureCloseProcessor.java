/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.processors;

import java.io.Closeable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;

import juast.Block;
import juast.CompilationUnit;
import juast.Expr;
import juast.IdExpr;
import juast.MethodInvExpr;
import juast.NewClassExpr;
import juast.ProblemReporter;
import juast.Stmt;
import juast.TryStmt;
import juast.VarDecl;
import juast.VarStmt;
import juast.process.AJuastProcessor;
import juast.visit.DefaultVisitor;

import org.apache.commons.lang.mutable.MutableBoolean;

import com.google.inject.Inject;

public class EnsureCloseProcessor extends AJuastProcessor {
	@Inject private ProblemReporter problemReporter;
	public static String errorMessage = "Closable was not properly closed";

	Set<String> exceptions = new HashSet<String>(Arrays.asList("java.io.StringWriter"));

	@Override
	public void afterAnalyse(final CompilationUnit cu) {
		cu.accept(new SuppressWarningsVisitor() {
			@Override
			public boolean enter(Block node) {
				List<Stmt> list = node.getStmts(); 
				for (int i=0; i<list.size(); i++) {
					Stmt s = list.get(i);
					if (AnnotationUtils.isWarningSupress(s)) {
						i++; // skip next stmt
						continue;
					}
					if (!(s instanceof VarStmt))
						continue;
					final VarDecl vd = ((VarStmt) s).getVar();
					if (AnnotationUtils.containName(vd.getAnnotations(), "SuppressWarnings"))
						continue;
					if (!(vd.getType() instanceof DeclaredType))
						continue;
					DeclaredType dt = (DeclaredType) vd.getType();
					if (dt == null)
						continue;
					String basetype = dt.asElement().toString();
					if (exceptions.contains(basetype))
						continue;
					{ // Is vd instanceof Closable ?
						TypeElement te = (TypeElement) dt.asElement();
						boolean ok = false;
						while (te != null) {
							for (TypeMirror tm : te.getInterfaces()) {
								DeclaredType iface = (DeclaredType) tm;
								String siface = iface.asElement().toString();
								if (Closeable.class.getName().equals(siface)) {
									ok = true;
									break;
								}
								if (exceptions.contains(siface))
									break;
							}
							TypeMirror t = te.getSuperclass();
							if (t.getKind() == TypeKind.NONE)
								break;
							te = (TypeElement) ((DeclaredType) t).asElement();
						}
						if (!ok)
							continue;
					}
					
					Expr e = vd.getInit();
					if (!(e instanceof NewClassExpr))
						continue;
					if (exceptions.contains(e.getResolvedType().toString()))
						continue;
					
					final MutableBoolean ok = new MutableBoolean(false);
					if (i+1 < list.size() && list.get(i+1) instanceof TryStmt) {
						TryStmt ts = (TryStmt) list.get(i+1);
						Block b = ts.getFinally();
						if (b != null)
							b.accept(new DefaultVisitor() {
								@Override
								public boolean enter(MethodInvExpr node) {
									if (node.getArgs().size()!=0)
										return true;
									if (!"close".equals( node.getMethod() ))
										return true;
									Expr e = node.getReceiver();
									if (!(e instanceof IdExpr))
										return true;
									// TODO: ensure in the same namespace
									IdExpr id = (IdExpr) e;
									if (vd.getVar().equals( id.getId() ))
										ok.setValue(true);
									
									return super.enter(node);
								}
							});
					}
					if (!ok.booleanValue())
						problemReporter.warning(cu, s, errorMessage);
				}
				return super.enter(node);
			}
		});
	}
}
