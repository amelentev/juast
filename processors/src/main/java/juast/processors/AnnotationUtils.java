/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.processors;

import java.util.List;

import juast.Annotation;
import juast.Expr;
import juast.ExprStmt;
import juast.IdExpr;
import juast.MethodInvExpr;
import juast.Stmt;

public class AnnotationUtils {
	public static boolean containName(List<Annotation> anns, String ann) {
		for (Annotation a : anns) {
			String s = a.getTypeName();
			if (s.equals(ann) || s.endsWith("."+ann))
				return true;
		}
		return false;
	}

	/**
	 * @return s == `Warnings.suppress()`
	 */
	public static boolean isWarningSupress(Stmt s) {
		// language~: s == ExprStmt(expr = MethodInvExpr(method:"suppress", receiver:IdExpr("Warnings")))
		// EDSL: if (new ExprStmt().setExpr(new MethodInvExpr().setMethod("suppress").setReceiver(new IdExpr("Warnings"))))
		//			.match(s))
		if (s instanceof ExprStmt) {
			Expr e = ((ExprStmt) s).getExpr();
			if (e instanceof MethodInvExpr) {
				MethodInvExpr mi = (MethodInvExpr) e;
				if ("suppress".equals(mi.getMethod())) {
					if (mi.getReceiver() instanceof IdExpr) {
						IdExpr id = (IdExpr) mi.getReceiver();
						if ("Warnings".equals(id.getId()))
							return true;
					}
				}
			}
		}
		return false;
	}
}
