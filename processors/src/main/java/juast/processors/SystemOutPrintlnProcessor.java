/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.processors;

import juast.CompilationUnit;
import juast.IdExpr;
import juast.MemberSelectExpr;
import juast.MethodInvExpr;
import juast.ProblemReporter;
import juast.process.AJuastProcessor;

import com.google.inject.Inject;

// TODO: import static System.out.println
public class SystemOutPrintlnProcessor extends AJuastProcessor {
	@Inject private ProblemReporter problemReporter;
	public static String errorMessage = "using System.out.println is bad practice";
	
	@Override
	public void afterParse(final CompilationUnit cu) {
		cu.accept(new SuppressWarningsVisitor() {
			@Override
			public boolean enter(MethodInvExpr node) {
				if (!"println".equals(node.getMethod()))
					return true;
				if (!(node.getReceiver() instanceof MemberSelectExpr))
					return true;
				MemberSelectExpr ms = (MemberSelectExpr) node.getReceiver();
				if (!"out".equals(ms.getId()))
					return true;
				if (!(ms.getExpr() instanceof IdExpr))
					return true;
				IdExpr id = (IdExpr) ms.getExpr();
				if (!"System".equals(id.getId()))
					return true;
				
				problemReporter.warning(cu, node, errorMessage);
				
				return true;
			}
		});
	}
}
