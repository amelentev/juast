/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.processors;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;

import juast.BinaryExpr;
import juast.Block;
import juast.CompilationUnit;
import juast.Expr;
import juast.IdExpr;
import juast.JuastTreeMaker;
import juast.MethodInvExpr;
import juast.NodeW;
import juast.TypeDecl;
import juast.VarDecl;
import juast.process.AJuastProcessor;
import juast.visit.DefaultVisitor;

import com.google.inject.Inject;

import fj.Ord;
import fj.data.Option;
import fj.data.TreeMap;

public class OperatorOverload extends AJuastProcessor {
	@Inject JuastTreeMaker tm;

	@Override
	public void afterParse(CompilationUnit cu) {
		cu.accept(new DefaultVisitor() {
			TreeMap<String, String> curnamespace = TreeMap.empty(Ord.stringOrd);
			Deque<TreeMap<String, String>> stacknamespace = new ArrayDeque<TreeMap<String,String>>();
			
			@Override
			public boolean enter(Block node) {
				stacknamespace.add(curnamespace);
				return super.enter(node);
			}
			@Override
			public boolean enter(TypeDecl node) {
				stacknamespace.add(curnamespace);
				return super.enter(node);
			}
			@Override
			public boolean leave(TypeDecl node) {
				curnamespace = stacknamespace.pop();
				return super.leave(node);
			}
			@Override
			public boolean leave(Block node) {
				curnamespace = stacknamespace.pop();
				return super.leave(node);
			}
			@Override
			public boolean enter(VarDecl node) {
				TypeMirror t = node.getType();
				if (t != null && t.getKind() == TypeKind.DECLARED) {
					curnamespace = curnamespace.set(node.getVar(), t.toString());
				}
				return super.enter(node);
			}
			java.util.Map<Expr, String> set = new IdentityHashMap<Expr, String>();

			boolean check(Expr e) {
				final List<String> supportedTypes = Arrays.asList("BigInteger", "BigDecimal");
				if (set.get(e) != null)
					return true;
				if (e instanceof IdExpr) {
					Option<String> cl = curnamespace.get(((IdExpr) e).getId());
					if (cl.isSome())
						if (supportedTypes.indexOf(cl.some())>=0) {
							set.put(e, cl.some());
							return true;
						}
				}
				return false;
			}
			@SuppressWarnings("serial")
			final Map<String, String> operators = new HashMap<String, String>() {{
				put("+", "add");
				put("-", "subtract");
				put("*", "multiply");
				put("/", "divide");
			}};
			@Override
			public boolean leave(BinaryExpr node) {
				String curop = node.getOperator();
				if (operators.get(curop) != null) {
					Expr l = node.getLeftOperand(),
						r = node.getRightOperand();
					if (check(l) || check(r)) {
						NodeW par = (NodeW) node.getParent();
						MethodInvExpr mie = tm.methodInvExpr(null,
								tm.memberSelectExpr(node.getLeftOperand(), operators.get(curop)),
								Arrays.asList(node.getRightOperand()));
						par.replace(node, mie);
						set.put(node, set.get(l));
					}
				}
				return super.leave(node);
			}
		});
	}
}
