/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.processors;

import static java.lang.Math.max;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.processing.Filer;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.SupportedOptions;
import javax.tools.FileObject;
import javax.tools.StandardLocation;

import juast.CompilationUnit;
import juast.Import;
import juast.ProblemReporter;
import juast.process.AJuastProcessor;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import com.google.inject.Inject;

/**
 * Check that there are no links from tracked in VCS (commited, added or modified) to untracked files.
 * Useful if you often forget to commit all needed files.
 * Works only for files inside compiler's "-sourcepath".
 */
@SupportedOptions({"vcs", "vcs.command", "vcs.untracked", "vcs.modified", "vcs.added"})
public class VCSProcessor extends AJuastProcessor {
	@Inject private ProblemReporter pr;
	@Inject private ProcessingEnvironment pe;
	@Inject private Filer filer;
	public static String errorMessage = "You forgot to commit imported class";

	@Override
	public void afterAnalyse(CompilationUnit unit) {
		VCSystem vcs = getVCS();
		File cur = new File(unit.getFileName());
		if (vcs.getStatus(cur.getParentFile(), cur) != VCSStatus.TRACKED)
			return;
		for (Import i : unit.getImports()) {
			String file = i.getName();
			if (i.isStatic())
				file = file.substring(0, file.lastIndexOf("."));
			// TODO: nested classes
			String pkg = file.substring(0, max(file.lastIndexOf("."), 0));
			file = file.substring(file.lastIndexOf(".")+1) + ".java";
			try {
				FileObject fo = filer.getResource(StandardLocation.SOURCE_PATH, pkg, file);
				file = fo.toUri().getPath();
			} catch (FileNotFoundException e) {
				// internal class. ignore
				return;
			} catch (IllegalArgumentException e) {
				// -sourcepath was not used. ignore
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			if (vcs.getStatus(cur.getParentFile(), new File(file)) == VCSStatus.UNTRACKED)
				pr.warning(unit, i, errorMessage);
		}
	}

	protected enum VCSStatus {
		TRACKED,
		UNTRACKED,
		UNKNOWN
	}

	static class VCSystem {
		String command, untracked, modified, added;

		VCSystem(String command, String untracked, String modified, String added) {
			this.command = command;
			this.untracked = untracked;
			this.modified = modified;
			this.added = added;
		}

		VCSStatus getStatus(File base, File file) {
			try {
				base = base.getAbsoluteFile();
				file = file.getAbsoluteFile();
				List<String> lst = new ArrayList<String>(Arrays.asList(command.split(" ")));
				lst.add(file.getPath());
				Process p = new ProcessBuilder(lst).directory(base).start();
				return p.waitFor()!=0
						? VCSStatus.UNKNOWN
						: getStatus(IOUtils.toString(p.getInputStream()));
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		protected VCSStatus getStatus(String output) {
			if (output.startsWith(untracked))
				return VCSStatus.UNTRACKED;
			if (output.length()==0 || output.startsWith(modified) || output.startsWith(added))
				return VCSStatus.TRACKED;
			return VCSStatus.UNKNOWN;
		}

		static VCSystem svn = new VCSystem("svn st", "? ", "M ", "A ");
		static VCSystem hg = new VCSystem("hg st", "? ", "M ", "A ");
		static VCSystem git = new VCSystem("git status -s", "?? ", "M ", "A ") {
			@Override
			protected VCSStatus getStatus(String output) {
				if (output.startsWith(" M ") || output.startsWith("MM "))
					return VCSStatus.TRACKED;
				return super.getStatus(output);
			};
		};
		@SuppressWarnings("serial")
		static Map<String, VCSystem> vcss = new HashMap<String, VCSProcessor.VCSystem>() {{
			put("svn", svn);
			put("hg", hg);
			put("git", git);
		}};
	}
	VCSystem getVCS() {
		// TODO: guess used vcs
		String vcs = pe.getOptions().get("vcs");
		if (vcs != null) {
			VCSystem res = VCSystem.vcss.get(vcs);
			if (res == null)
				throw new RuntimeException("Unknown vcs ["+vcs+"]");
			return res;
		}
		String command = StringUtils.defaultString(pe.getOptions().get("vcs.command"), "hg st"),
			untracked = StringUtils.defaultString(pe.getOptions().get("vcs.untracked"), "? "),
			modified = StringUtils.defaultString(pe.getOptions().get("vcs.modified"), "M "),
			added = StringUtils.defaultString(pe.getOptions().get("vcs.added"), "A ");
		return new VCSystem(command, untracked, modified, added);
	}
}
