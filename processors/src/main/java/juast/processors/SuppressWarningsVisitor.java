/**
 * Copyright (C) Artem Melentyev
 * Licensed under the Educational Community License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.osedu.org/licenses/ECL-2.0
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package juast.processors;

import juast.BodyDecl;
import juast.Node;
import juast.Stmt;
import juast.Warnings;
import juast.visit.DefaultVisitor;

/**
 * AST Visitor which skip nodes marked as @{@link SuppressWarnings} annotation and {@link Warnings#suppress()} stmt.
 */
public class SuppressWarningsVisitor extends DefaultVisitor {
	private boolean suppresNext = false;

	@Override
	public boolean defaultLeave(Node node) {
		suppresNext = (node instanceof Stmt && AnnotationUtils.isWarningSupress((Stmt) node));
		return super.defaultLeave(node);
	}
	@Override
	public boolean defaultEnter(Node node) {
		if (suppresNext)
			return false;
		if (node instanceof BodyDecl)
			return (!AnnotationUtils.containName(((BodyDecl) node).getAnnotations(), "SuppressWarnings"));
		return super.defaultEnter(node);
	}
}
